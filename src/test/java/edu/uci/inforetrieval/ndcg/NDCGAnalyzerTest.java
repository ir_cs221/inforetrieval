package edu.uci.inforetrieval.ndcg;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.uci.inforetrieval.search.SearchService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/cs221-servlet.xml")
public class NDCGAnalyzerTest{

	@Autowired
	private SearchService searchService;
	
	private NDCGAnalyzer ndcgAnalyzer;
	
	@Before
	public void setUp(){
		ndcgAnalyzer = new NDCGAnalyzer();
		ndcgAnalyzer.setSearchService(searchService);
	}
	
	@Test
	public void testAnalyzeNDCG() throws UnsupportedEncodingException, IOException{
		ndcgAnalyzer.analyzeNDCG();
	}
	
	@Test
	public void testGetGoogleResults() throws UnsupportedEncodingException, IOException{
		Map<String, Integer> resultMap = ndcgAnalyzer.getGoogleResults(QueryConstants.INFORMATION_RETRIEVAL, 10);
		System.out.println(resultMap);
	}
	
}
