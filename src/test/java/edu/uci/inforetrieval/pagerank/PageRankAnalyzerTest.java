package edu.uci.inforetrieval.pagerank;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.uci.inforetrieval.dao.PageDAO;
import edu.uci.inforetrieval.pagerank.PageRankAnalyzer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/cs221-servlet.xml")
public class PageRankAnalyzerTest {
	
	private PageRankAnalyzer pageRankAnalyzer;
	
	@Autowired
	private PageDAO pageDAO;
	
	@Before
	public void setUp(){
		pageRankAnalyzer = new PageRankAnalyzer();
	}
	
	@Test
	public void testCalculatePageRank(){
		pageRankAnalyzer.calculatePageRank(null);
	}
	
}
