package edu.uci.inforetrieval.crawler;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.Before;
import org.junit.Test;

public class ControllerTest {
	
	private static final Logger LOG = 
			Logger.getLogger(ControllerTest.class);

	private Controller controller;
	
	@Before
	public void setUp(){
		controller = new Controller();
	}
	
	@Test
	public void testCrawl() throws Exception{
		controller.crawl();
	}
	
	@Test
	public void testLogging(){
		LOG.info("test");
	}
	
}
