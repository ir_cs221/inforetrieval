package edu.uci.inforetrieval.crawler;

import java.net.MalformedURLException;

import org.junit.Before;
import org.junit.Test;

import edu.uci.inforetrieval.dao.DocumentDAO;
import edu.uci.inforetrieval.dao.PageDAO;
import edu.uci.inforetrieval.pagerank.AnchorTextAnalyzer;
import edu.uci.inforetrieval.pagerank.PageRankAnalyzer;
import edu.uci.inforetrieval.textprocessing.Utils;

public class DocumentBuilderTest {
	
	private DocumentBuilder docBuilder;
	
	@Before
	public void setUp(){
		docBuilder = new DocumentBuilder();
		docBuilder.setPageDAO(new PageDAO());
		docBuilder.setDocumentDAO(new DocumentDAO());
		docBuilder.setUtils(new Utils());
		docBuilder.setAnchorTextAnalyzer(new AnchorTextAnalyzer());
		docBuilder.setPageRankAnalyzer(new PageRankAnalyzer());
	}
	
	@Test
	public void testPopulateDocumentTable() throws MalformedURLException{
		docBuilder.populateDocumentTable();
	}
	
	@Test
	public void testUpdateDocuments_addAnchorTag(){
		docBuilder.updateDocuments_addAnchorText();
		System.out.println("Successfully added the anchor text");
	}
	
	@Test
	public void testRemoveDuplicates(){
		docBuilder.removeDuplicates();
	}
	
	@Test
	public void testRemoveInformaticsHost() throws MalformedURLException{
		docBuilder.removeInformaticsHost();
	}
	
	@Test
	public void testUpdatePageRank(){
		docBuilder.updateDocuments_addPageRank();
	}
	
	
}
