package edu.uci.inforetrieval.search;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import edu.uci.inforetrieval.dao.DocumentDAO;
import edu.uci.inforetrieval.dao.IndexDAO;
import edu.uci.inforetrieval.dao.PageDAO;
import edu.uci.inforetrieval.textprocessing.Utils;

public class SearchServiceTest {
	private SearchService searchService;
	
	@Before
	public void setUp(){
		searchService = new SearchService();
		IndexDAO indexDAO = new IndexDAO();
		indexDAO.setDocumentDAO(new DocumentDAO());
		searchService.setIndexDAO(indexDAO);
		searchService.setUtils(new Utils());
	}
	
	@Test
	public void testGetRelevantDocuments() throws IOException{
		String searchQuery = "student affairs";
		long start = System.currentTimeMillis();
		int maxResults = 5;
		List<SearchResult> results = searchService.getRelevantDocuments(searchQuery, maxResults);
		long end = System.currentTimeMillis();
		System.out.println("Time taken to give search results:"+(end - start) +" ms");
		System.out.println("Num results:"+results.size());
		for (int i = 0; i < maxResults; i++) {
			SearchResult result = results.get(i);
			System.out.println(result.getScore());
			System.out.println(result.getTitle());
			System.out.println(result.getURL());
			System.out.println();
			System.out.println();
		}
	}
}
