package edu.uci.inforetrieval.textprocessing;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class AnagramTest{
	
	private Anagram anagram;
	private String wordFile;
	private String mapSerFile;
	private Utils utils;
	private String inputFileName;
	private String sysoutFile;
	
	@Before
	public void setUp() throws FileNotFoundException{
		wordFile = "src/test/resources/words/words_5L.txt";
		mapSerFile = "src/test/resources/output/anagramMap.ser";
		inputFileName = "src/test/resources/pg100.txt";
		sysoutFile = "src/test/resources/output/SystemOutput.txt";
		anagram = new Anagram();
		utils = new Utils();
	}
	
	@Test
	public void testCreateWordNetAnagramsMap() throws IOException{
		long start = System.currentTimeMillis();
		anagram.createWordNetAnagramsMap(wordFile, mapSerFile );
		long diff = System.currentTimeMillis() - start;
		System.out.println("Time taken for creating anagramMap:"+diff+" ms");
	}
	
	@Test
	public void testReadAnagramMap() throws FileNotFoundException, ClassNotFoundException, IOException{
		long start = System.currentTimeMillis();
		Map<String, List<String>> anagramMap = anagram.readAnagramMap(mapSerFile);
		long diff = System.currentTimeMillis() - start;
		System.out.println("Time Taken to load Anagram Map:"+diff +" ms");
		System.out.println("Map Size:"+anagramMap.size());
		Iterator<String> iterator = anagramMap.keySet().iterator();
		int counter = 0;
		while(iterator.hasNext()){
			String key = iterator.next();
			List<String> anagrams = anagramMap.get(key);
			System.out.println(key+"-->"+anagrams);
			counter++;
			if(counter == 100){
				break;
			}
		}
		
	}
	
	@Test
	public void testGetAnagrams() throws IOException, ClassNotFoundException{
		long start = System.currentTimeMillis();
		List<String> tokens = utils.tokenizeFile(inputFileName);
		long diff1 = System.currentTimeMillis() - start;
		System.out.println("Time taken to Tokenize File:"+diff1+" ms");
		
		Map<String, List<String>> map = anagram.getAnagrams(tokens , mapSerFile, wordFile);
		long diff2 = System.currentTimeMillis() - start - diff1;
		System.out.println("Time taken to get anagrams:"+diff2+" ms");
		System.out.println("Map Size:"+map.size());
		UtilsTest.setSystemOut(sysoutFile);
		anagram.print(map);
	}
	
	@Test
	public void testGetPermutations(){
		String sample = "sandep";
		List<String> anagrams = anagram.getPermutations(sample);
		System.out.println("Size:"+anagrams.size());
		System.out.println(anagrams);
	}
	
	@Test
	public void testProcessWords() throws IOException{
		anagram.processWords("resources/words/words_6L.txt", wordFile);
	}
}

