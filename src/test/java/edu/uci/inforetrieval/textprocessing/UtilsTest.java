package edu.uci.inforetrieval.textprocessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import edu.uci.inforetrieval.analysis.StopWords;

public class UtilsTest{
	
	private Utils utils;
	private Anagram anagram;
	private String inputFileName;
	private String sysoutFile;
	private String mapSerFile;
	private String wordFile;
	private static PrintStream stdout = System.out;
	private String htmlContent;
	private static final String SAMPLE_HTML = "src/test/resources/sample.html";
	
	@Before
	public void setUp() throws IOException{
		utils = new Utils();
		anagram = new Anagram();
		inputFileName = "src/test/resources/pg100.txt";
		sysoutFile = "src/test/resources/output/SystemOutput.txt";
		wordFile = "src/test/resources/words/words_5L.txt";
		mapSerFile = "src/test/resources/output/anagramMap.ser";
		
		BufferedReader bufferedReader = new BufferedReader(new FileReader(SAMPLE_HTML));
		StringBuilder builder = new StringBuilder();
		String nextLine = null;
		while((nextLine = bufferedReader.readLine()) != null){
			builder.append(nextLine);
		}
		htmlContent = builder.toString();
		bufferedReader.close();
	}
	
	@Test
	public void testAll() throws IOException, ClassNotFoundException{
		long start = System.currentTimeMillis();
		List<String> tokens = utils.tokenizeFile(inputFileName);
		Map<String, Integer> freqMap = utils.computeWordFrequencies(tokens);
		Map<String, Integer> threeGramFreqMap = utils.computeThreeGramFrequencies(tokens);
		Map<String, List<String>> map = anagram.getAnagrams(tokens , mapSerFile, wordFile);
		long diff = System.currentTimeMillis() - start;
		System.out.println("Time Taken:" + diff + " ms");
	}
	
	@Test
	public void testTokenizeFile() throws IOException{
		long start = System.currentTimeMillis();
		List<String> tokens = utils.tokenizeFile(inputFileName);
		long diff = System.currentTimeMillis() - start;
		System.out.println("Time Taken to tokenize file:" + diff + " ms");
		System.out.println("Number of Tokens:"+tokens.size());
		setSystemOut(sysoutFile);
		utils.printTokens(tokens);
		
		
	}
	
	@Test
	public void testComputeWordFrequencies() throws IOException{
		long start = System.currentTimeMillis();
		List<String> tokens = utils.tokenizeFile(inputFileName);
		long diff1 = System.currentTimeMillis() - start;
		System.out.println("Time Taken to tokenize file:" + diff1 + " ms");
		
		
		Map<String, Integer> freqMap = utils.computeWordFrequencies(tokens);
		long diff2 = System.currentTimeMillis() - start - diff1;
		System.out.println("Time Taken to Compute Frequencies:" +diff2 + " ms");
		setSystemOut(sysoutFile);
		utils.printFreqMap(freqMap);
	}
	
	@Test
	public void testComputeThreeGramFrequencies() throws IOException{
		long start = System.currentTimeMillis();
		List<String> tokens = utils.tokenizeFile(inputFileName);
		long diff1 = System.currentTimeMillis() - start;
		System.out.println("Time Taken to tokenize file:" + diff1 + " ms");
		
		Map<String, Integer> freqMap = utils.computeThreeGramFrequencies(tokens);
		long diff2 = System.currentTimeMillis() - start - diff1;
		System.out.println("Time Taken to Compute Frequencies:" +diff2 + " ms");
		setSystemOut(sysoutFile);
		utils.printFreqMap(freqMap);
	}
	
	public static void setSystemOut(String sysoutFile) 
			throws FileNotFoundException{
		PrintStream printStream = null;
		File file = new File(sysoutFile);
		printStream = new PrintStream(new FileOutputStream(file));
		System.setOut(printStream);

	}
	
	public static void clearSystemOutFile(){
		System.setOut(stdout);
	}
	

	@Test
	public void testGetText() throws IOException{
		
		String text = utils.getText(htmlContent);
		System.out.println(text);
	}
	
	@Test
	public void testGetWords() throws IOException{
		String text = utils.getText(htmlContent);
		List<String> words = utils.getWords(text);
		System.out.println("Words List Size:"+words.size());
		System.out.println(words);
	}
	
	@Test
	public void testGet3Grams() throws IOException{
		String text = utils.getText(htmlContent);
		List<String> threeGramList = utils.get3Grams(text);
		System.out.println("ThreeGrams Size:"+ threeGramList.size());
		System.out.println(threeGramList);
	}
	
	@Test
	public void testStemWord(){
		System.out.println(utils.stemWord("information"));
	}
	
	@Test
	public void testGetURLContent() throws MalformedURLException{
		String urlString = "http://www.ics.uci.edu/~lopes/teaching/cs221W16/";
		String urlContent = utils.getURLContent(urlString);
		System.out.println("URLContent:"+urlContent);
	}
	@Test
	public void testGetStopWords() throws IOException{
		StopWords.getStopWords();
	}
}
