package edu.uci.inforetrieval.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.mutable.MutableInt;
import org.junit.Before;
import org.junit.Test;

import edu.uci.inforetrieval.index.TermDocument;
import edu.uci.inforetrieval.index.IndexBuilder;
import edu.uci.inforetrieval.index.Term;

public class IndexDAOTest {
	private IndexDAO indexDAO;
	
	@Before
	public void setUp(){
		indexDAO = new IndexDAO();
	}
	
	@Test
	public void testInsertIndexMap(){
		Map<String, Term> indexMap = new HashMap<String, Term>();
		indexDAO.insertIndexMap(indexMap);
	}
	
	@Test
	public void testGetDocuments(){
		MutableInt docFreq = new MutableInt();
		List<TermDocument> list = indexDAO.getTermDocuments("inform", 10, docFreq );
		for(TermDocument doc : list){
			System.out.println(doc);
		}
	}
}
