package edu.uci.inforetrieval.dao;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import edu.uci.inforetrieval.dao.PageDAO;
import static edu.uci.inforetrieval.crawler.MongoDBConstants.*;

public class PageDAOTest {
	
	private PageDAO pageDAO;
	public static final String HTML_DUMP_FILE = "src/test/resources/htmldump_backup_6.txt";
	
	@Before
	public void setUp() throws IOException{
		pageDAO = new PageDAO();		
	}
		
	@Test
	public void testInsertIntoDBFromFile() throws IOException{
		pageDAO.insertIntoDBFromFile(HTML_DUMP_FILE);
		System.out.println("Finished Inserting into DB");
	}
	
	@Test
	public void testDeletePages(){
		pageDAO.deletePages("sample URL");
	}
	

}
