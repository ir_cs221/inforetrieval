package edu.uci.inforetrieval.dao;

import static edu.uci.inforetrieval.crawler.MongoDBConstants.ANCHOR_TEXT_LIST;
import static edu.uci.inforetrieval.crawler.MongoDBConstants.DOC_ID;
import static edu.uci.inforetrieval.crawler.MongoDBConstants.URL;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import edu.uci.inforetrieval.crawler.Document;

public class DocumentDAOTest {
	private DocumentDAO documentDAO;
	
	@Before
	public void setUp() throws IOException{
		documentDAO = new DocumentDAO();		
	}
	
	@Test
	public void testGetDocuments(){
		DBCursor dbCursor = documentDAO.getDocumentDBCursor();
		int limit = 100;
		while(dbCursor.hasNext()){
			limit--;
			DBObject dbObject = dbCursor.next();
			int docID = (int) dbObject.get(DOC_ID);
			String url = (String) dbObject.get(URL);
			List<String> anchorTextList = (List<String>) dbObject.get(ANCHOR_TEXT_LIST);
			System.out.println("DocID:"+docID+"; URL:"+ url+"; Anchor text List:"+ anchorTextList);
			if(limit == 0){
				break;
			}
		}		
	}
	
	@Test
	public void testGetAllDocs(){
		List<Document> docs = documentDAO.getAllDocs();
		double maxPageRank = 0;
		for(Document doc : docs){
			if(maxPageRank < doc.getPageRank()){
				maxPageRank = doc.getPageRank();
			}
		}
		System.out.println("maxPageRank:"+maxPageRank);
	}
	
}
