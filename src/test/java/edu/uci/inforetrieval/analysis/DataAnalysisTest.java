package edu.uci.inforetrieval.analysis;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import edu.uci.inforetrieval.analysis.DataAnalysis;
import edu.uci.inforetrieval.analysis.DataStatistics;
import edu.uci.inforetrieval.dao.PageDAO;
import edu.uci.inforetrieval.dao.PageDAOTest;
import edu.uci.inforetrieval.textprocessing.Utils;

public class DataAnalysisTest {
	
	private DataAnalysis dataAnalysis;
	
	@Before
	public void setUp(){
		dataAnalysis = new  DataAnalysis();
		dataAnalysis.setPageDAO(new PageDAO());
		dataAnalysis.setUtils(new Utils());
	}
	
	@Test
	public void testGetDataStatistics() throws IOException{
		DataStatistics dataStatistics = dataAnalysis.getDataStatistics();
		dataAnalysis.analyzeData(dataStatistics);
		System.out.println("Data Analyzed");
	}
	
	@Test
	public void testGetDataStatistics_htmldump() throws IOException{
		DataStatistics dataStatistics = dataAnalysis.getDataStatistics_htmldump(PageDAOTest.HTML_DUMP_FILE);
		dataAnalysis.analyzeData(dataStatistics);
		System.out.println("Data Analyzed");
	}
}
