package edu.uci.inforetrieval.index;

import java.io.IOException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.tartarus.martin.Stemmer;

import edu.uci.inforetrieval.dao.DocumentDAO;
import edu.uci.inforetrieval.dao.IndexDAO;
import edu.uci.inforetrieval.dao.PageDAO;
import edu.uci.inforetrieval.textprocessing.Utils;

public class IndexBuilderTest {
	
	private IndexBuilder indexBuilder;
	
	@Before
	public void setUp(){
		indexBuilder = new IndexBuilder();
		indexBuilder.setUtils(new Utils());
		indexBuilder.setDocumentDAO(new DocumentDAO());
		indexBuilder.setIndexDAO(new IndexDAO());
	}
	
	@Test
	public void testBuildIndexMap() throws IOException{
		long start = System.currentTimeMillis();
		int maxDocs = 100;
		indexBuilder.buildIndexMap(maxDocs);
		Map<String, Term> map = indexBuilder.getIndexMap();
		long end = System.currentTimeMillis();
		System.out.println(map.size());
		System.out.println("Time Taken to Build Index Map:"+ (end - start ) +" ms");
	}
	
	@Test
	public void testStemming(){
		Stemmer stemmer = new Stemmer();
		String s = "Simple";
		stemmer.add(s.toCharArray(), s.length());
		stemmer.stem();
		System.out.println(stemmer.toString());
	}
	
	@Test
	public void testInsertIndexIntoDB() throws IOException{
		long start = System.currentTimeMillis();
		int maxDocs = -1;
		indexBuilder.insertIndexIntoDB(maxDocs);
		System.out.println("Successfully inserted the index into MongoDB");
		long end = System.currentTimeMillis();
		System.out.println("Time Taken to insert index into MongoDB:"+ (end - start ) +" ms");
	}
}
