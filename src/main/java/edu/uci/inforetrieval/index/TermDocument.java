package edu.uci.inforetrieval.index;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import edu.uci.inforetrieval.textprocessing.Utils;

public class TermDocument implements Serializable, Comparable<TermDocument> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5742404063313346693L;
	
	private int docID;
	private String url;
	private String title;
	private int termFrequency;
	private List<Integer> termPositions;
	private Double tf_idf;
	private String relevantText;
	//The index of the first occurrence of the term
	private int termIndex;
	//Storing anchor text list here so that we need not query document table again for this field
	private List<String> anchorTextList;
	private double pageRank;
	
	public String getRelevantText() {
		return relevantText;
	}

	public void setRelevantText(String relevantText) {
		this.relevantText = relevantText;
	}

	public void setDocID(int id) {
		this.docID = id;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setTermFrequency(int termFrequency) {
		this.termFrequency = termFrequency;
	}

	public void setTermPositions(List<Integer> termPositions) {
		this.termPositions = termPositions;
	}

	public TermDocument(String url, int docID){
		this.url = url;
		this.docID = docID;
		termPositions = new ArrayList<Integer>();
	}
	
	public int getDocID() {
		return docID;
	}
	public String getUrl() {
		return url;
	}
	public int getTermFrequency() {
		return termFrequency;
	}
	public void incrementTermFrequency() {
		this.termFrequency++;
	}
	public List<Integer> getTermPositions() {
		return termPositions;
	}
	public void addTermPosition(int termPosition) {
		this.termPositions.add(termPosition);
	}	

	@Override
	public String toString() {
		return "Document [id=" + docID + ", url=" + url + ", termFrequency="
				+ termFrequency + ", termPositions=" + termPositions
				+ ", tf_idf=" + tf_idf + "]";
	}
	
	static class DocumentComparator implements Comparator<TermDocument>{

		@Override
		public int compare(TermDocument d1, TermDocument d2) {
			return d1.getTermFrequency() - d2.getTermFrequency();
		}
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + docID;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermDocument other = (TermDocument) obj;
		if (docID != other.docID)
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public int compareTo(TermDocument document) {
		Double thisTf_idf = this.tf_idf;
		Double otherTf_idf = document.getTf_idf();
		return otherTf_idf.compareTo(thisTf_idf);
	}

	public double getTf_idf() {
		return tf_idf;
	}
	
	public void updateTf_idf(int docFrequency) {
		tf_idf = Utils.calculateTF_IDF(termFrequency, docFrequency);
	}
	
	public void setTf_idf(Double tf_idf) {
		this.tf_idf = tf_idf;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getTermIndex() {
		return termIndex;
	}

	public void setTermIndex(int termIndex) {
		this.termIndex = termIndex;
	}

	public List<String> getAnchorTextList() {
		return anchorTextList;
	}

	public void setAnchorTextList(List<String> anchorTextList) {
		this.anchorTextList = anchorTextList;
	}
	
	public void setPageRank(double pageRank) {
		this.pageRank = pageRank;
	}
	
	public double getPageRank() {
		return pageRank;
	}
}
