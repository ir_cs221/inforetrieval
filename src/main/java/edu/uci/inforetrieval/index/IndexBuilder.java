package edu.uci.inforetrieval.index;

import static edu.uci.inforetrieval.crawler.MongoDBConstants.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import edu.uci.inforetrieval.dao.DocumentDAO;
import edu.uci.inforetrieval.dao.IndexDAO;
import edu.uci.inforetrieval.dao.PageDAO;
import edu.uci.inforetrieval.textprocessing.Utils;

public class IndexBuilder {
	
	private Map<String, Term> indexMap;
	private IndexDAO indexDAO;
	private DocumentDAO documentDAO;
	private Utils utils;
	
	public void setIndexDAO(IndexDAO indexDAO) {
		this.indexDAO = indexDAO;
	}
	
	public void setDocumentDAO(DocumentDAO documentDAO) {
		this.documentDAO = documentDAO;
	}
	
	public void setUtils(Utils utils) {
		this.utils = utils;
	}
	
	public IndexBuilder(){
		indexMap = new  HashMap<String, Term>();
	}
	
	public Map<String, Term> getIndexMap() {
		return indexMap;
	}
	
	/**
	 * 
	 * @param documentLimit --> -1 implies there is no limit
	 * @throws IOException
	 */
	public void buildIndexMap(int documentLimit) throws IOException{
		DBCursor dbCursor = documentDAO.getDocumentDBCursor();
		int corpusSize = 0;
		while(dbCursor.hasNext()){
			DBObject dbObject = dbCursor.next();
			String urlString = (String) dbObject.get(URL);
			String htmlContent = (String) dbObject.get(HTML_CONTENT);
			String title = (String) dbObject.get(TITLE);
			String textContent = (String) dbObject.get(TEXT_CONTENT);
			String urlContent = (String) dbObject.get(URL_CONTENT);
			List<String> anchorTextList = (List<String>) dbObject.get(ANCHOR_TEXT_LIST);;
			double pageRank = 0;
			if( dbObject.get(PAGE_RANK) != null){
				pageRank = (double) dbObject.get(PAGE_RANK);
			};
			int docId = (int) dbObject.get(DOC_ID);
			corpusSize ++;
			List<String> words = utils.getWords(textContent);
			words.addAll(utils.getWords(title)); //TODO give more importance to title during ranking
			words.addAll(utils.getWords(urlContent));//TODO give more importance to url content during ranking
			if (anchorTextList!=null) {
				for (String anchorText : anchorTextList) {
					words.addAll(utils.getWords(anchorText));
				}
			}
			for (int position = 0; position < words.size(); position++) {
				
				String word = words.get(position);
				int index = textContent.indexOf(word); //TODO use the delimiters later
				word = word.toLowerCase(); //TODO should we ignore the case?
				word = utils.stemWord(word);
				Term term = indexMap.get(word);
				if(term == null){
					term = new Term(word);
					indexMap.put(word, term);
					
				}
				Map<String, TermDocument> urlVsDocuments = term.getUrlVsDocuments();
				TermDocument termDocument = urlVsDocuments.get(urlString);
				if(termDocument == null){
					termDocument = new TermDocument(urlString, docId);
					termDocument.setTitle(title);
					termDocument.setTermIndex(index);
					termDocument.setAnchorTextList(anchorTextList);
					termDocument.setPageRank(pageRank);
					urlVsDocuments.put(urlString, termDocument);
					term.addDocument(termDocument);
					
				}
				termDocument.incrementTermFrequency();
				termDocument.addTermPosition(position);
				termDocument.updateTf_idf(urlVsDocuments.size());
			}
			if(documentLimit > 0 && corpusSize == documentLimit){
				break;
			}
		}
		System.out.println("Built the Index: Corpus Size is:"+corpusSize);
	}
	
	public void insertIndexIntoDB(int maxDocs) throws IOException{
		buildIndexMap(maxDocs);
		Map<String, Term> map = getIndexMap();
		System.out.println("Index Map Size:"+map.size());
		indexDAO.insertIndexMap(map);
	}
	
}
