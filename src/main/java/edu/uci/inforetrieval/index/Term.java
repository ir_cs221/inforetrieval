package edu.uci.inforetrieval.index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import edu.uci.inforetrieval.comparators.TermFrequencyComparator;


public class Term {
	
	private int id;
	private String word;
	//Defined Map to retrieve the TermDocument Based on URL. Used while building the index
	private Map<String, TermDocument> urlVsdocuments;
	//List of all the documents this term is present in
	private List<TermDocument> documents;
	private static int numTerms;
	
	public Term(String word){
		this.word = word;
		this.urlVsdocuments = new HashMap<String, TermDocument>();
		this.documents = new ArrayList<TermDocument>();
		this.id = ++numTerms;
	}
	
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + ((word == null) ? 0 : word.hashCode());
//		return result;
//	}
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		Term other = (Term) obj;
//		if (word == null) {
//			if (other.word != null)
//				return false;
//		} else if (!word.equals(other.word))
//			return false;
//		return true;
//	}
	
	
	public int getId() {
		return id;
	}
	public String getWord() {
		return word;
	}
	public Map<String, TermDocument> getUrlVsDocuments() {
		return urlVsdocuments;
	}
	public List<TermDocument> getDocuments() {
		Collections.sort(documents);
		return documents;
	}
	@Override
	public String toString() {
		return "Term [id=" + id + ", word=" + word + "]";
	}

	public void addDocument(TermDocument document) {
		documents.add(document);
	}
	
}
