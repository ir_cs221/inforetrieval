package edu.uci.inforetrieval.crawler;

public class MongoDBConstants {
	public static final String DB_NAME = "IR_CS221_SHA";
	public static final String DB_HOST = "localhost";//"128.195.6.153";
	public static final int DB_PORT = 27017;
	//Tables
	public static final String PAGE_TABLE_NAME = "Webpage";
	public static final String TERM_TABLE_NAME = "Term";
	public static final String DOCUMENT_TABLE_NAME = "Document";
	
	//Webpage table column headers
//	public static final String URL = "url";
//	public static final String HTML_CONTENT = "htmlcontent";
//	public static final String TEXT_LENGTH = "textlength";
	
	//Term Table Column headers
	public static final String TERM_ID = "term_id";
	public static final String WORD = "word";
	public static final String INDEX = "index";
	public static final String DOCUMENTS = "documents";
	
	//Document Table Column headers
	public static final String DOC_ID = "doc_id";
	public static final String HTML_CONTENT = "htmlcontent";
	public static final String TEXT_LENGTH = "textlength";
	public static final String TITLE = "title";
	public static final String URL = "url";
	public static final String TEXT_CONTENT = "textcontent";
	public static final String URL_CONTENT = "urlcontent";
	public static final String ANCHOR_TEXT_LIST = "anchor_text_list";
	public static final String PAGE_RANK = "page_rank";
	
	public static final String TERM_FREQUENCY = "term_frequency";
	public static final String TERM_POSITIONS = "term_positions";
	public static final String TF_IDF = "tf_idf";
	
	public static final String FILE_DUMP = "htmldump.txt";
	
	public static final String INCREMENT_OPERATOR = "$inc";
	public static final String SET_OPERATOR = "$set";
	
	public static final String DELIMITER = "--------------------------------------------------------------------";
	
//	static{
//		try {
//			DB_HOST = InetAddress.getLocalHost().getHostAddress();
//		} catch (UnknownHostException e) {
//			e.printStackTrace();
//		}
//	}
	}
