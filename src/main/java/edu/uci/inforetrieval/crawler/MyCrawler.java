package edu.uci.inforetrieval.crawler;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;





import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import edu.uci.inforetrieval.dao.PageDAO;

public class MyCrawler extends WebCrawler{
	
	private static final Logger LOG = Logger.getLogger(MyCrawler.class);
	private static int numPagesVisited;
	private PageDAO pageDAO;
	
	public static final Pattern FILTERS = 
			Pattern.compile(".*(\\.(css|js|gif|jpg|png|mp3|zip|gz|pdf|doc|docx|ppt|pptx|xsl|xslx|exe|csv|data|jar|java))$");
	public static final String ICS_DOMAIN_SUFFIX = "ics.uci.edu";
	private static final String DUTTGROUP = "duttgroup.ics.uci.edu";
	private static final String CALENDAR = "calendar.ics.uci.edu/";
	private static final int MAX_URL_LENGTH = 255;
	public static final String ARCHIVE_HOST = "archive.ics.uci.edu";
	
	public MyCrawler() throws UnknownHostException{
		pageDAO = new PageDAO();
	}
	
	/**
     * This method receives two parameters. The first parameter is the page
     * in which we have discovered this new url and the second parameter is
     * the new url. You should implement this function to specify whether
     * the given url should be crawled or not (based on your crawling logic).
     * In this example, we are instructing the crawler to ignore urls that
     * have css, js, git, ... extensions and to only accept urls that start
     * with "http://www.ics.uci.edu/". In this case, we didn't need the
     * referringPage parameter to make the decision.
     */
	@Override
	public boolean shouldVisit(Page referringPage, WebURL url){
		String href = url.getURL().toLowerCase();
//		LOG.info("URL:"+href);
		System.out.println("URL:"+href);
		String subDomain = url.getSubDomain();
		String domain = url.getDomain();
		URL url2 = null;
		String query = null;
		try {
			url2 = new URL(href);
			query = url2.getQuery();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		String hostName = null;
		if(subDomain == null || subDomain.trim().isEmpty()){
			hostName = domain;
		}else{
			hostName = subDomain + "." + domain;
		}
//		LOG.info("HostName:"+hostName);
		System.out.println("HostName:"+hostName);
		
		if(!hostName.endsWith(ICS_DOMAIN_SUFFIX)){
//			LOG.warn("IGNORE_URL: Non ICS domain:" + hostName);
			System.out.println("IGNORE_URL: Non ICS domain: " + hostName);
			return false;
		}
		if(hostName.endsWith(DUTTGROUP)){
//			LOG.warn("IGNORE_URL: DUTTGROUP domain:" + href);
			System.out.println("IGNORE_URL: DUTTGROUP domain: " + href);
			return false;
		}
		if(hostName.endsWith(ARCHIVE_HOST)){
//			LOG.warn("IGNORE_URL: DUTTGROUP domain:" + href);
			System.out.println("IGNORE_URL: ARCHIVE domain: " + href);
			return false;
		}
		if(hostName.endsWith(CALENDAR)){
			System.out.println("IGNORE_URL: CALENDAR domain: " + href);
			return false;
		}
		if(FILTERS.matcher(href).matches()){
//			LOG.warn("IGNORE_URL: Unnecessary Files" + href);
			System.out.println("IGNORE_URL: Unnecessary Files: " + href);
			return false;
		}
		//To avoid never ending paths
		if(href.length() > MAX_URL_LENGTH){
			System.out.println("IGNORE_URL: MAX_LENGTH exceeded: " + href);
			return false;
		}
		
		if(query!= null && query.contains("&")){
			int ampersandCount = 0;
			for (int i = 0; i < query.length(); i++) {
				if(query.charAt(i) == '&'){
					ampersandCount++;
					if(ampersandCount > 2 ){
						System.out.println("IGNORE_URL: Has more than 2 Query Paramaters:"+href);
						return false;
					}
				}
			}
		}
		
        return true;
	}
	
	/**
     * This function is called when a page is fetched and ready
     * to be processed by your program.
     */
	@Override
	public void visit(Page page) {
		try {
			String url = page.getWebURL().getURL();
			System.out.println("Visited the Page : " + url);

			if (page.getParseData() instanceof HtmlParseData) {
			    HtmlParseData htmlParseData = (HtmlParseData) page.getParseData();
			    String text = htmlParseData.getText();
			    String html = htmlParseData.getHtml();
			    Set<WebURL> links = htmlParseData.getOutgoingUrls();

//            LOG.info("Text length: " + text.length());
//            LOG.info("Html length: " + html.length());
//            LOG.info("Number of outgoing links: " + links.size());
			    System.out.println("Text length: " + text.length());
			    System.out.println("Html length: " + html.length());
			    System.out.println("Number of outgoing links: " + links.size());
			    
				numPagesVisited ++;
//		        LOG.info("Num Pages Visited so far: "+numPagesVisited);
				System.out.println("Num Pages Visited so far: "+numPagesVisited);
				pageDAO.insertPage(url, html, text.length());
//				pageDAO.insertIntoFile(url, html);
			}
			
//        doc.put("TextL", arg1)
			

		} catch (Exception e) {
			System.out.println("Exception while visiting page and storing in mongoDB: "+e.getMessage());
			throw e;
		}
	}

}
