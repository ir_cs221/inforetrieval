package edu.uci.inforetrieval.crawler;

import org.apache.log4j.Logger;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

public class Controller {
	
	private static final Logger LOG = Logger.getLogger(Controller.class);
	private static final int NUM_CRAWLERS = 7;
	private static final String SEED_URL_1 = "http://www.ics.uci.edu";
	
	public void crawl() throws Exception{
		try {

			CrawlConfig config = prepareCrawlConfig();

			/*
			 * Instantiate the controller for this crawl.
			 */
			PageFetcher pageFetcher = new PageFetcher(config);
			RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
			RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
			CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);

			/*
			 * For each crawl, you need to add some seed urls. These are the first
			 * URLs that are fetched and then the crawler starts following links
			 * which are found in these pages
			 */
			controller.addSeed(SEED_URL_1);

			/*
			 * Start the crawl. This is a blocking operation, meaning that your code
			 * will reach the line after this only when crawling is finished.
			 */
			controller.start(MyCrawler.class, NUM_CRAWLERS);
		} catch (Exception e) {
//			LOG.error(e.getMessage(), e);
			e.printStackTrace();
			throw e;
		}
	}
	
	public CrawlConfig prepareCrawlConfig(){
		CrawlConfig crawlConfig = new CrawlConfig();
		crawlConfig.setUserAgentString(CrawlConfigConstants.USER_AGENT_STRING);
		crawlConfig.setPolitenessDelay(CrawlConfigConstants.POLITENESS_DELAY);
		crawlConfig.setMaxDepthOfCrawling(CrawlConfigConstants.MAX_DEPTH_OF_CRAWLING);
		crawlConfig.setMaxPagesToFetch(CrawlConfigConstants.MAX_PAGES_TO_FETCH);
		crawlConfig.setResumableCrawling(CrawlConfigConstants.RESUMABLE_CRAWLING);
		crawlConfig.setCrawlStorageFolder(CrawlConfigConstants.CRAWL_STORAGE_FOLDER);
		
//		crawlConfig.setProxyHost(CrawlConfigConstants.PROXY_HOST);
//		crawlConfig.setProxyPort(CrawlConfigConstants.PROXY_PORT);
//		crawlConfig.setProxyUsername(CrawlConfigConstants.PROXY_USERNAME);
//		crawlConfig.setProxyPassword(CrawlConfigConstants.PROXY_PASSWORD);
		return crawlConfig;
	}
}
