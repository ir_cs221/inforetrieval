package edu.uci.inforetrieval.crawler;

import static edu.uci.inforetrieval.crawler.MongoDBConstants.*;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import edu.uci.inforetrieval.dao.DocumentDAO;
import edu.uci.inforetrieval.dao.PageDAO;
import edu.uci.inforetrieval.pagerank.AnchorTextAnalyzer;
import edu.uci.inforetrieval.pagerank.PageRankAnalyzer;
import edu.uci.inforetrieval.textprocessing.Utils;

public class DocumentBuilder {
	private PageDAO pageDAO;
	private DocumentDAO documentDAO;
	private Utils utils;
	private AnchorTextAnalyzer anchorTextAnalyzer;
	private PageRankAnalyzer pageRankAnalyzer;
	
	public void setPageDAO(PageDAO pageDAO) {
		this.pageDAO = pageDAO;
	}
	
	public void setDocumentDAO(DocumentDAO documentDAO) {
		this.documentDAO = documentDAO;
	}
	
	public void setPageRankAnalyzer(PageRankAnalyzer pageRankAnalyzer) {
		this.pageRankAnalyzer = pageRankAnalyzer;
	}
	
	public void setUtils(Utils utils) {
		this.utils = utils;
	}
	
	public void setAnchorTextAnalyzer(AnchorTextAnalyzer anchorTextAnalyzer) {
		this.anchorTextAnalyzer = anchorTextAnalyzer;
	}
	@Deprecated
	public void populateDocumentTable() throws MalformedURLException{
		DBCursor dbCursor = pageDAO.getPageDBCursor();
		int numDocsInserted = 0;
		List<Document> documents = new ArrayList<Document>();
		while(dbCursor.hasNext()){
			DBObject dbObject = dbCursor.next();
			String urlString = (String) dbObject.get(URL);
			if(!utils.isValidPage(urlString)){
				continue;
			}
			String htmlContent = (String) dbObject.get(HTML_CONTENT);
			String title = utils.getTitle(htmlContent);
			String text = utils.getText(htmlContent);
			String urlContent = utils.getURLContent(urlString);
			
			Document document = new Document();
			document.setHtmlContent(htmlContent);
			document.setTextContent(text);
			document.setTextLength(text.length());
			document.setTitle(title);
			document.setUrl(urlString);
			document.setUrlContent(urlContent);
			
			documents.add(document);
			
		}
		
		anchorTextAnalyzer.populateAnchorText(documents);
		for(Document doc : documents){
			documentDAO.insertDocument(doc);
			numDocsInserted ++;
		}
		
		System.out.println("Inserted Documents into Document Table: numDocs:"+numDocsInserted);
	}
	
	public void updateDocuments_addAnchorText(){
		DBCursor cursor = documentDAO.getDocumentDBCursor();
		List<Document> docs = new ArrayList<Document>();
		while(cursor.hasNext()){
			DBObject dbObject = cursor.next();
			String urlString = (String) dbObject.get(URL);
			String htmlContent = (String) dbObject.get(HTML_CONTENT);
			int docID = (Integer) dbObject.get(DOC_ID);
			Document document = new Document();
			document.setHtmlContent(htmlContent);
			document.setUrl(urlString);
			document.setDocID(docID);
			
			docs.add(document);
		}
		anchorTextAnalyzer.populateAnchorText(docs);
		for(Document doc : docs){
			documentDAO.updateDocument_addAnchorTextList(doc);
		}
	}
	
	public void removeDuplicates(){
		DBCursor cursor = documentDAO.getDocumentDBCursor();
		Set<String> urls = new HashSet<String>();
		while(cursor.hasNext()){
			DBObject dbObject = cursor.next();
			String urlString = (String) dbObject.get(URL);
			urlString = Utils.removeProtocol(urlString);
			urlString = Utils.removeWWW(urlString);
			if(urlString.matches(".*(index\\.(php|html))$")){
//				System.out.println(urlString);
				urlString = urlString.substring(0, urlString.lastIndexOf('/') + 1);
			}
			int docID = (Integer) dbObject.get(DOC_ID);
			if(urls.contains(urlString)){
				System.out.println("Found the Duplicate URL:"+urlString);
				documentDAO.deleteDoc(docID); 
			}else{
				urls.add(urlString);
			}
		}
	}
	
	public void removeInformaticsHost() throws MalformedURLException{
		DBCursor cursor = documentDAO.getDocumentDBCursor();
		while(cursor.hasNext()){
			DBObject dbObject = cursor.next();
			String urlString = (String) dbObject.get(URL);
			String host = Utils.getHost(urlString);
			if(host.toLowerCase().endsWith("informatics.uci.edu")){
				System.out.println("Found the informatics URL:"+urlString);
				int docID = (Integer) dbObject.get(DOC_ID);
				documentDAO.deleteDoc(docID); 
//				System.out.println(host);
				
			}
			
		}
	}
	
	public void updateDocuments_addPageRank(){
		List<Document> docs = documentDAO.getAllDocs();
		pageRankAnalyzer.calculatePageRank(docs);
		for (Document doc : docs) {
			System.out.println("Inserting page rank "+ doc.getPageRank() + " for "+doc.getUrl());
			documentDAO.updateDocument_addPageRank(doc);
		}
	}
}
