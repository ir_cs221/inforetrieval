package edu.uci.inforetrieval.crawler;

import java.util.ArrayList;
import java.util.List;

public class Document {
	private static int numDocs;
	private int docID;
	private String url;
	private String htmlContent;
	private String textContent;
	private int textLength;
	private String title;
	private String urlContent;
	private double pageRank = 1.0;
	private int numOutGoingLinks;
	private List<String> incomingDocs = new ArrayList<String>();
	private List<String> anchorTextList;
	
	public Document(){
		numDocs++;
		this.setDocID(numDocs);
	}

	public int getDocID() {
		return docID;
	}

	public void setDocID(int docID) {
		this.docID = docID;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getHtmlContent() {
		return htmlContent;
	}

	public void setHtmlContent(String htmlContent) {
		this.htmlContent = htmlContent;
	}

	public String getTextContent() {
		return textContent;
	}

	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}

	public int getTextLength() {
		return textLength;
	}

	public void setTextLength(int textLength) {
		this.textLength = textLength;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrlContent() {
		return urlContent;
	}

	public void setUrlContent(String urlContent) {
		this.urlContent = urlContent;
	}

	public double getPageRank() {
		return pageRank;
	}

	public void setPageRank(double pageRank) {
		this.pageRank = pageRank;
	}

	public List<String> getAnchorTextList() {
		return anchorTextList;
	}

	public void setAnchorTextList(List<String> anchorTextList) {
		this.anchorTextList = anchorTextList;
	}

	public int getNumOutGoingLinks() {
		return numOutGoingLinks;
	}

	public void setNumOutGoingLinks(int numOutGoingLinks) {
		this.numOutGoingLinks = numOutGoingLinks;
	}

	public List<String> getIncomingDocs() {
		return incomingDocs;
	}

	public void setIncomingDocIds(List<String> incomingDocs) {
		this.incomingDocs = incomingDocs;
	}
	
	public void addIncomingDoc(String url){
		this.incomingDocs.add(url);
	}
}
