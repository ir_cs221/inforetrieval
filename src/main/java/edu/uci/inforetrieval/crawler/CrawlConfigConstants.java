package edu.uci.inforetrieval.crawler;

public class CrawlConfigConstants {
	public static final int POLITENESS_DELAY = 500;	
	public static final String USER_AGENT_STRING = "IR W16 WebCrawler 76321572 89681430 14208253";
	public static final int MAX_DEPTH_OF_CRAWLING = -1;
	public static final int MAX_PAGES_TO_FETCH = -1;
	public static final boolean RESUMABLE_CRAWLING = true;
	public static final String CRAWL_STORAGE_FOLDER = "data/crawl/root";
	
	public static final String PROXY_HOST = "smadugul@openlab.ics.uci.edu";
	public static final int PROXY_PORT = 8080;
	public static final String PROXY_USERNAME = "smadugul";
	public static final String PROXY_PASSWORD = "";
}
