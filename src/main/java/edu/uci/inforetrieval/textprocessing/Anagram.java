package edu.uci.inforetrieval.textprocessing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import edu.uci.inforetrieval.comparators.KeyComparator;

public class Anagram {
	
	/**
	 * For creating an offline map of word vs listOfAngarms
	 * Called during initialization phase
	 * @param wordsfile
	 * @param mapSerFile
	 * @return
	 * @throws IOException
	 */
	public Map<String, List<String>> createWordNetAnagramsMap(String wordsfile, String mapSerFile) throws IOException{
		BufferedReader bufferedReader = null;
		ObjectOutputStream oos = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(wordsfile));
			String word = null;
			Map<String, List<String>> anagramMap = new HashMap<String, List<String>>();
			while((word = bufferedReader.readLine()) != null){
				if(word.contains("'")){
					continue;
				}
				word = word.toLowerCase();
				String sortedWord = sort(word);
				List<String> anagrams = anagramMap.get(sortedWord);
				if(anagrams == null){
					anagrams = new ArrayList<String>();
					anagramMap.put(sortedWord, anagrams);
				}
				anagrams.add(word);
			}
//			System.out.println("Map Size Before Removing single anagrams:"+anagramMap.size());
//			Iterator<String> iterator = anagramMap.keySet().iterator();
//			while(iterator.hasNext()){
//				String key = iterator.next();
//				if(anagramMap.get(key).size() == 1){
//					iterator.remove();
//				}
//				
//			}
//			System.out.println("Map Size After Removing single anagrams:"+anagramMap.size());
			if (mapSerFile != null) {
				oos = new ObjectOutputStream(new FileOutputStream(mapSerFile));
				oos.writeObject(anagramMap);
			}
			return anagramMap;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally{
			if(bufferedReader != null){
				bufferedReader.close();
			}
			if(oos != null){
				oos.close();
			}
		}
		
	}
	
	/**
	 * For removing the apostophe's and the duplicates from the word list.
	 * Called only once for initialization
	 * @param inputFileName
	 * @param outputFileName
	 * @throws IOException
	 */
	public void processWords(String inputFileName, String outputFileName) 
			throws IOException{
		BufferedReader bufferedReader = null;
		BufferedWriter bufferedWriter = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(inputFileName));
			bufferedWriter = new BufferedWriter(new FileWriter(outputFileName));
			String word = null;
			TreeSet<String> treeSet = new TreeSet<String>(new KeyComparator());
			while((word = bufferedReader.readLine()) != null){
				if(!word.contains("'")){
					treeSet.add(word.toLowerCase());
				}
			}
			for(String current : treeSet){
				bufferedWriter.write(current+"\n");
			}
			bufferedWriter.flush();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		}finally{
			if(bufferedReader != null){
				bufferedReader.close();
			}
			if(bufferedWriter != null){
				bufferedWriter.close();
			}
		}
	}
	
	public Map<String, List<String>> getAnagrams(List<String> tokens, String mapSerFile, String wordsFile) 
			throws FileNotFoundException, ClassNotFoundException, IOException{
		Map<String, List<String>> tokenVsAnagrams = new HashMap<String, List<String>>();
		Set<String> tokenSet = new HashSet<String>(tokens);
//		Map<String, List<String>> anagramMap = readAnagramMap(mapSerFile);
		Map<String, List<String>> anagramMap = createWordNetAnagramsMap(wordsFile, null);
		for (String token : tokenSet) {
			List<String> anagrams = anagramMap.get(sort(token));
			List<String> anagramsNew = removeTokenFromList(anagrams, token);
			if(anagramsNew != null && !anagramsNew.isEmpty()){
				tokenVsAnagrams.put(token, anagramsNew);
				
			}
		}		
		return tokenVsAnagrams;
	}
	
	/**
	 * Removes the given token from the given list
	 * @param anagrams
	 * @param token
	 * @return
	 */
	private List<String> removeTokenFromList(List<String> anagrams, String token) {
		if(anagrams == null){
			return null;
		}
		List<String> newAnagramList = new ArrayList<String>();
		for(String current : anagrams){
			if(!token.equalsIgnoreCase(current)){
				newAnagramList.add(current);
			}
		}
		return newAnagramList;
	}

	private String sort(String token) {
		char[] charArray = token.toCharArray();
		Arrays.sort(charArray);
		String sortedWord = new String(charArray);
		return sortedWord;
	}
	
	public void print(Map<String, List<String>> anagramMap){
		Comparator<String> keyComparator = new KeyComparator();
		Map<String, List<String>> treeMap = new TreeMap<String, List<String>>(keyComparator);
		treeMap.putAll(anagramMap);
		Iterator<String> iterator = treeMap.keySet().iterator();
		while(iterator.hasNext()){
			String key = iterator.next();
			System.out.println(key+"-->"+treeMap.get(key));
		}
		System.out.println();
	}
	
	/**
	 * The method is not used
	 * @param token
	 * @return
	 */
	public List<String> getPermutations(String token){
		List<String> list = new ArrayList<String>();
		char[] charArray = token.toCharArray();
		doPermutation(list, charArray, charArray.length);
		return list; 
		
	}
	
	/**
	 * The method is not used
	 * @param token
	 * @return
	 */
	private void doPermutation(List<String> list, char[] charArray, int newSize) {
		if(newSize == 1){
			return;
		}
		for (int i = 0; i < newSize; i++) {
			doPermutation(list, charArray, newSize - 1);
			if(newSize == 2){
				list.add(new String(charArray));
				System.out.println(new String(charArray));
			}
			changeOrder(charArray, newSize);
		}
		
	}
	
	/**
	 * The method is not used
	 * @param token
	 * @return
	 */
	private void changeOrder(char[] charArray, int newSize) {
		int pointAt = charArray.length - newSize;
		char temp = charArray[pointAt];
		for (int i = pointAt + 1; i < charArray.length; i++) {
			charArray[i - 1] = charArray[i];
		}
		charArray[charArray.length - 1] = temp;
	}

	class ValueComparator implements Comparator<String>{

		
		private Map<String, List<String>> map;
		
		public ValueComparator(Map<String, List<String>> map) {
			this.map = map;
		}

		@Override
		public int compare(String key1, String key2) {
			int key2ListSize = map.get(key2).size();
			int key1ListSize = map.get(key1).size();
			int retValue = Integer.valueOf(key2ListSize).compareTo(key1ListSize);
			if(retValue != 0){
				return retValue;
			}else{
				return key2.compareTo(key1);
			}
			
		}
		
	}

	public Map<String, List<String>> readAnagramMap(String mapSerFile) 
			throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(mapSerFile));
			Map<String, List<String>> anagramMap = (Map<String, List<String>>)ois.readObject();
			return anagramMap;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			if(ois != null){
				ois.close();
			}
		}
	}
	
}
