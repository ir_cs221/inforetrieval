package edu.uci.inforetrieval.textprocessing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.springframework.stereotype.Service;
import org.tartarus.martin.Stemmer;

import edu.uci.inforetrieval.analysis.StopWords;
import edu.uci.inforetrieval.comparators.ValueComparator;
import edu.uci.inforetrieval.crawler.MyCrawler;

@Service
public class Utils {
	
	private static final int CORPUS_SIZE = 54427;
	
	private static Set<String> urls = new HashSet<String>();
	private Stemmer stemmer = new Stemmer();

	public List<String> tokenizeFile(String fileName) throws IOException{
		BufferedReader reader = null;
		try {
			List<String> tokens = new ArrayList<String>();
			FileReader fileReader = new  FileReader(fileName);
//			InputStream inputStream = Utils.class.getResourceAsStream(fileName);
			reader = new BufferedReader(fileReader);
			String line;
			while((line = reader.readLine()) != null){
				line = line.trim();
				line = line.toLowerCase();
				if(line.isEmpty()){
					continue;
				}
				String[] words = line.replaceAll("[^a-zA-Z0-9\\s]", "").split("\\s+");
				tokens.addAll(Arrays.asList(words));
//				for(String word: words){
//					String processedString = processWord(word);
//					if(!word.isEmpty()){
//						tokens.add(processedString);
//					}
//					
//				}
				
			}
			
			return tokens;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			if(reader != null){
				reader.close();
			}
		}
		
	}
	
	/**
	 * To be used in the next projects
	 * @param word
	 * @return
	 */
	private String processWord(String word) {
		word = word.trim();
		word = word.toLowerCase();
		word = word.replaceAll("[^a-zA-Z0-9]", "");
		return word;
	}

	public void printTokens(List<String> tokens){
		System.out.println("Number of Tokens:"+tokens.size());
		for (String token : tokens) {
			System.out.println(token);
		}
	}
	
	public Map<String, Integer> computeWordFrequencies(List<String> tokens){
		Map<String, Integer> wordFreqMap = new HashMap<String, Integer>();
		for (String token : tokens) {
			Integer freq = wordFreqMap.get(token);
			if (freq == null){
				freq = 1;
			}else{
				freq ++;
			}
			wordFreqMap.put(token, freq);
		}
		
		return wordFreqMap;
	}
	
	public void printFreqMap(Map<String, Integer> freqMap){
		Comparator<String> comparator = new ValueComparator(freqMap);
		Map<String, Integer> treeMap = new TreeMap<String, Integer>(comparator);
		treeMap.putAll(freqMap);
		
		System.out.println("Number of Unique Tokens:"+treeMap.size());
		Iterator<String> iterator = treeMap.keySet().iterator();
		while(iterator.hasNext()){
			String key = iterator.next();
			System.out.println(key+"-->"+treeMap.get(key));
		}
	}
	
	public Map<String, Integer> computeThreeGramFrequencies(List<String> tokens){
		Map<String, Integer> threGramFreqMap = new HashMap<String, Integer>();
		for (int count = 0; count < tokens.size() - 3; count++) {
			String threeGram = tokens.get(count) + " " + tokens.get(count + 1) + " " + tokens.get(count + 2);
			Integer freq = threGramFreqMap.get(threeGram);
			if (freq == null){
				freq = 1;
			}else{
				freq ++;
			}
			threGramFreqMap.put(threeGram, freq);
		}
		
		
		return threGramFreqMap;
	}
	
	//Added during Crawler project (project2)
	public List<String> getWords(String text) throws IOException{
		text = text.replaceAll("[^a-zA-Z0-9'\\s]", ""); //Keeping singleQuote(') so that it cant be removed in stopwords
		String[] words = text.split("\\s+");
		List<String> wordsList = new ArrayList<String>();
		for (String word : words) {
			word = word.trim();
			if(word.isEmpty() || word.length() == 1){
				continue;
			}
			//Ignore numbers
			if(word.matches("\\d+")){
				continue;
			}
			wordsList.add(word);
		}
		Set<String> stopwords = StopWords.getStopWords();
		wordsList.removeAll(stopwords);
		return wordsList;
		
	}
	//Added during Crawler project (project2)
	public List<String> get3Grams(String text) throws IOException{
		List<String> words = getWords(text);
		return get3Grams(words);
	}
	//Added during Crawler project (project2)
	public List<String> get3Grams(List<String> processedWords) throws IOException{
		List<String> threeGrams = new ArrayList<String>();
		for (int count = 0; count < processedWords.size() - 2; count++) {
			threeGrams.add(processedWords.get(count)+ " " + processedWords.get(count + 1) 
					+ " " + processedWords.get(count + 2));
		}
		return threeGrams;
	}
	
	public String getTitle(String htmlContent){
		Document document = Jsoup.parse(htmlContent);
		return document.title();
	}
	
	public String getURLContent(String urlString) throws MalformedURLException{
		URL url = new URL(urlString);
		String path = url.getPath();
		String host = url.getHost();
		String[] hostArray = host.split("[^a-zA-Z0-9]");
		String[] pathArray = path.split("[^a-zA-Z0-9]");
		StringBuilder builder = new StringBuilder();
		//Ignore com, edu, org etc. Consider only length - 1 elements
		for (int i = 0; i < hostArray.length - 1; i++) {
			if("www".equalsIgnoreCase(hostArray[i])){
				continue;
			}
			builder.append(hostArray[i]+" ");
		}
		for(String curr : pathArray){
			builder.append(curr+" ");
		}
		return builder.toString();
	}
	
	//Added during Crawler project (project2)
	public String getText(String htmlContent){
		Document document = Jsoup.parse(htmlContent);
		
		return getText(document);
		
	}
	//Added during Crawler project (project2)
	private String getText(Element parentElement) {
	     String working = "";
	     for (Node child : parentElement.childNodes()) {
	          if (child instanceof TextNode) {
	              working += ((TextNode)child).text() + " ";
	          }
	          if (child instanceof Element) {
	              Element childElement = (Element)child;                  
	              working += getText(childElement);
	          }
	     }

	     return working;
	}
	
	public boolean isValidPage(String urlString) throws MalformedURLException{
		if(urls.contains(urlString)){
			//Duplicate URL. Ignore
			return false;
		}

		if(!urlString.startsWith("http")){
			return false;
		}
		if(MyCrawler.FILTERS.matcher(urlString).matches()){
			return false;
		}
		URL url = new URL(urlString);
		
		String host = url.getHost();
		if(host.contains(MyCrawler.ARCHIVE_HOST)){
			return false;
		}
		
		urls.add(urlString);
		return true;
		
	}
	
	public List<String> getWordsFromHTML(String htmlContent) throws IOException{
		return getWords(getText(htmlContent));
	}

	public String stemWord(String word) {
		stemmer.add(word.toCharArray(), word.length());
		stemmer.stem();
		return stemmer.toString();
	}

	public static double calculateTF_IDF(int termFrequency, int docFrequency) {
		//(1 + log(tf)) * log(N/df)
		return (1 + Math.log(termFrequency)) * (Math.log((double)CORPUS_SIZE/docFrequency));
	}
	
	public static String removeProtocol(String url){
		if(url.startsWith("https://")){
			return url.substring(8);
		}
		if(url.startsWith("http://")){
			return url.substring(7);
		}
		return url;
	}
	
	public static String removeWWW(String url){
		if(url.startsWith("www.")){
			return url.substring(4);
		}
		return url;
	}
	
	public static String getHost(String urlString) throws MalformedURLException{
		URL url = new URL(urlString);
		return url.getHost();
	}
	
	public static String normalizeURL(String url){
		url = url.toLowerCase();
		url = removeProtocol(url);
		url = removeWWW(url);
		//Convert the urls http://ics.uci.edu/index.html to http://ics.uci.edu/
		if(url.matches(".*(index\\.(php|html))$")){
//			System.out.println(urlString);
			url = url.substring(0, url.lastIndexOf('/') + 1);
		}
		return url;
	}

}
