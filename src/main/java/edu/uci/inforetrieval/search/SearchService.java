package edu.uci.inforetrieval.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.mutable.MutableInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.uci.inforetrieval.dao.IndexDAO;
import edu.uci.inforetrieval.index.TermDocument;
import edu.uci.inforetrieval.textprocessing.Utils;

@Service
public class SearchService {
	
	private static final int MAX_DOCS_TO_CONSIDER = 50;
	private static final double PAGE_RANK_WEIGHT = 2;
	private static final double COSINE_RANK_WEIGHT = 1;
	private static final double MAX_PAGE_RANK =  1838.3653135134755;
	
	@Autowired
	private Utils utils;
	
	@Autowired
	private IndexDAO indexDAO;
	
	public void setUtils(Utils utils) {
		this.utils = utils;
	}
	
	public void setIndexDAO(IndexDAO indexDAO) {
		this.indexDAO = indexDAO;
	}
	
	/**
	 * Some Heuristics Used:
	 * 1. Double the score when the word is present in the url
	 * 2. Triple the score when the word is present in the title or anchor text list
	 * @param searchQuery
	 * @param maxResults
	 * @return
	 * @throws IOException
	 */
	public List<SearchResult> getRelevantDocuments(String searchQuery, int maxResults) throws IOException{
		List<SearchResult> results = new ArrayList<SearchResult>();
		Map<String, SearchResult> urlVsSearchResult = new HashMap<String, SearchResult>(); 
		List<String> words = utils.getWords(searchQuery);
		Map<String, Integer> wordVsFreq = getWordVsFrequency(words);
		Map<String, Double> queryTermVsTF_IDF = new LinkedHashMap<String, Double>();
		for (Entry<String, Integer> entry : wordVsFreq.entrySet()) {
			String word = entry.getKey();
			int freq = entry.getValue();
			MutableInt docFreq = new MutableInt();
			List<TermDocument> termDocs = indexDAO.getTermDocuments(word, MAX_DOCS_TO_CONSIDER, docFreq);
			double queryTermTfidf = Utils.calculateTF_IDF(freq, docFreq.intValue());
			queryTermVsTF_IDF.put(word, queryTermTfidf);
			for(TermDocument termDocument : termDocs){
				String url = termDocument.getUrl();
				SearchResult result = urlVsSearchResult.get(url);
				if(result == null){
					result = new SearchResult();
					result.setDocID(termDocument.getDocID());
					String title = termDocument.getTitle();
					if(title == null || title.isEmpty()){
						title = termDocument.getUrl();
					}
					result.setTitle(title);
					result.setURL(termDocument.getUrl());
					result.setRelevantText(termDocument.getRelevantText());
					result.setPageRank(termDocument.getPageRank());
					results.add(result);
					urlVsSearchResult.put(url, result);
				}
				result.addWordVsTfidf(word, termDocument.getTf_idf());
				double multiplier = getMultplierFactor(termDocument, word);
				result.setMultiplier(multiplier);
//				result.addToScore(1 * termDocument.getTf_idf());
			}
		}

		updateCosineScore(queryTermVsTF_IDF, results);
		
		
		Collections.sort(results);
		if(results.size() <= maxResults){
			return results;
		}
		return results.subList(0, maxResults) ;
	}

	private void updateCosineScore(Map<String, Double> queryTermVsTF_IDF,
			List<SearchResult> results) {
		double queryVectorNormalizedValue = getNormalizedValue(queryTermVsTF_IDF.values());
		
		for (SearchResult result : results) {
			Map<String, Double> wordVsTfidf = result.getWordVsTfidf();
			double normalizedValue = getNormalizedValue(wordVsTfidf.values());
			double cosineScore = 0;
			for(Entry<String, Double> entry : wordVsTfidf.entrySet()){
				Double queryTfIdf = queryTermVsTF_IDF.get(entry.getKey());
				if(queryTfIdf != null){
					cosineScore += entry.getValue() * queryTfIdf;
				}
			}
			cosineScore = cosineScore / (queryVectorNormalizedValue * normalizedValue);
			double pageRankScore = calculatePageRank(result.getPageRank());
			cosineScore = result.getMultiplier() * COSINE_RANK_WEIGHT* cosineScore;
			result.addToScore(pageRankScore + cosineScore);
		}
		
	}
	private double calculatePageRank(double pageRank) {
//		double pageRankScore = PAGE_RANK_WEIGHT * (Math.log(pageRank) / Math.log(MAX_PAGE_RANK));
		double pageRankScore = PAGE_RANK_WEIGHT * (pageRank / MAX_PAGE_RANK);
		if(pageRankScore < 0){
			return 0;
		}
		return pageRankScore;
	}

	/**
	 * Calculates the root of the sum of the squares of the values
	 * @param values
	 * @return
	 */
	private double getNormalizedValue(Collection<Double> values) {
		double normalizedValue = 0;
		for(Double tfIdf : values){
			normalizedValue += tfIdf * tfIdf;
		};
		normalizedValue = Math.sqrt(normalizedValue);
		return normalizedValue;
	}

	private Map<String, Integer> getWordVsFrequency(List<String> words) {
		Map<String, Integer> retMap = new LinkedHashMap<String, Integer>();
		for(String word : words){
			word = utils.stemWord(word);
			word = word.toLowerCase();
			Integer freq = retMap.get(word);
			if(freq == null){
				freq = 0;
			}
			freq += 1;
			retMap.put(word, freq);
		}
		return retMap;
	}

	private double getMultplierFactor(TermDocument termDoc, String word) throws IOException {
		List<String> titleWords = utils.getWords(termDoc.getTitle());
		for(String tword : titleWords){
			tword = utils.stemWord(tword);
			if(tword.equalsIgnoreCase(word)){
				return 3;
			}
		}
		
		List<String> urlWords = utils.getWords(termDoc.getUrl());
		for(String urlWord : urlWords){
			urlWord = utils.stemWord(urlWord);
			if(urlWord.equalsIgnoreCase(word)){
				return 2;
			}
		}
		List<String> anchorTextList = termDoc.getAnchorTextList();
		if(anchorTextList != null){
			for(String anchorText : anchorTextList){
				List<String> aWords = utils.getWords(anchorText);
				for(String aWord : aWords){
					aWord = utils.stemWord(aWord);
					if(aWord.equalsIgnoreCase(word)){
						return 3;
					}
				}
			}
		}
		
		return 1;
	}
}
