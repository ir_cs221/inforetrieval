package edu.uci.inforetrieval.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SearchResult implements Comparable<SearchResult>{
	private String URL;
	private int docID;
	private String title;
	private String relevantText;
	private double score;
	//this stores the tfidf values of the query terms in order
	//this is used to calculate the cosine similarity
	@JsonIgnore
	private Map<String, Double> wordVsTfidf = new HashMap<String, Double>(); 
	
	//Used to increase the score in case the word occurs in the title or url
	@JsonIgnore
	private double multiplier;
	@JsonIgnore
	private double pageRank;
	
	
	public double getScore() {
		return score;
	}
	public void addToScore(double score) {
		this.score += score;
	}
	public double getMultiplier() {
		return multiplier;
	}
	public void setMultiplier(double multiplier) {
		if(multiplier > this.multiplier){
			this.multiplier = multiplier;
		}
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRelevantText() {
		return relevantText;
	}
	public void setRelevantText(String relevantText) {
		this.relevantText = relevantText;
	}
	public int getDocID() {
		return docID;
	}
	public void setDocID(int docID) {
		this.docID = docID;
	}
	public Map<String, Double> getWordVsTfidf() {
		return wordVsTfidf;
	}
	public void addWordVsTfidf(String word, Double tfidf) {
		this.wordVsTfidf.put(word, tfidf);
	}
	public void setPageRank(double pageRank) {
		this.pageRank = pageRank;
	}
	public double getPageRank() {
		return pageRank;
	}
	@Override
	public int compareTo(SearchResult o2) {
		Double thisScore = this.getScore();
		Double otherScore = o2.getScore();
		return otherScore.compareTo(thisScore);
	}
	
	
	
}
