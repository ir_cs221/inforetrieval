package edu.uci.inforetrieval.ndcg;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class QueryConstants {
	
	public static final int HIGH_RELEVANCE = 3;
	public static final int MODERATE_RELEVANCE = 2;
	public static final int LOW_RELEVANCE = 1;
	public static final int NO_RELEVANCE = 0;
	public static final String MONDEGO = "mondego";
	public static final String MACHINE_LEARNING = "machine learning";
	public static final String SOFTWARE_ENGINEERING = "software engineering";
	public static final String SECURITY = "security";
	public static final String STUDENT_AFFAIRS = "student affairs";
	public static final String GRADUATE_COURSES = "graduate courses";
	public static final String CRISTA_LOPES = "crista lopes";
	public static final String REST = "REST";
	public static final String COMPUTER_GAMES = "computer games";
	public static final String INFORMATION_RETRIEVAL = "information retrieval";
	public static final String[] SAMPLE_QUERIES = {MONDEGO, MACHINE_LEARNING, 
		SOFTWARE_ENGINEERING, SECURITY, STUDENT_AFFAIRS, GRADUATE_COURSES, 
		CRISTA_LOPES, REST, COMPUTER_GAMES, INFORMATION_RETRIEVAL};
	
	public static final Map<String, Map<String, Integer>> GOOGLE_RESULT_MAP = new HashMap<String, Map<String,Integer>>();
	
	static{
		Map<String, Integer> mondegoResults = new LinkedHashMap<String, Integer>();
		mondegoResults.put("http://mondego.ics.uci.edu/", HIGH_RELEVANCE);
		mondegoResults.put("http://nile.ics.uci.edu:9000/wifi", HIGH_RELEVANCE);
		mondegoResults.put("http://nile.ics.uci.edu:9000/wifi/user/account/", HIGH_RELEVANCE);
		mondegoResults.put("http://mondego.ics.uci.edu/projects/clonedetection/", MODERATE_RELEVANCE);
		mondegoResults.put("http://nile.ics.uci.edu:9000/wifi/admin/estates/new", MODERATE_RELEVANCE);
		mondegoResults.put("http://nile.ics.uci.edu:9000/wifi/forgotpassword", MODERATE_RELEVANCE);
		mondegoResults.put("http://www.ics.uci.edu/~lopes/", LOW_RELEVANCE);
		mondegoResults.put("http://sdcl.ics.uci.edu/2012/05/calico-for-the-mondego-group/", LOW_RELEVANCE);
		mondegoResults.put("http://mondego.ics.uci.edu/projects/SourcererCC/", LOW_RELEVANCE);
		mondegoResults.put("http://mondego.ics.uci.edu/datasets/wikipedia-events/", LOW_RELEVANCE);
		GOOGLE_RESULT_MAP.put(MONDEGO, mondegoResults );
		
		Map<String, Integer> mlResults = new LinkedHashMap<String, Integer>();
		mlResults.put("http://archive.ics.uci.edu/ml/", HIGH_RELEVANCE);
		mlResults.put("https://archive.ics.uci.edu/ml/datasets.html", HIGH_RELEVANCE);
		mlResults.put("http://mlearn.ics.uci.edu/MLRepository.html", HIGH_RELEVANCE);
		mlResults.put("https://archive.ics.uci.edu/ml/machine-learning-databases/", MODERATE_RELEVANCE);
		mlResults.put("https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/", MODERATE_RELEVANCE);
		mlResults.put("https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/", MODERATE_RELEVANCE);
		mlResults.put("https://archive.ics.uci.edu/ml/about.html", LOW_RELEVANCE);
		mlResults.put("https://archive.ics.uci.edu/ml/datasets.html?format=&task=reg&att=&area=&numAtt=&numIns=&type=&sort=nameUp&view=table", LOW_RELEVANCE);
		mlResults.put("http://sli.ics.uci.edu/Classes/2015W-273a", LOW_RELEVANCE);
		mlResults.put("https://archive.ics.uci.edu/ml/citation_policy.html", LOW_RELEVANCE);
		GOOGLE_RESULT_MAP.put(MACHINE_LEARNING, mlResults);
		
		Map<String, Integer> seResults = new LinkedHashMap<String, Integer>();
		seResults.put("http://www.ics.uci.edu/prospective/en/degrees/software-engineering/", HIGH_RELEVANCE);
		seResults.put("https://www.ics.uci.edu/ugrad/degrees/degree_se.php", HIGH_RELEVANCE);
		seResults.put("https://www.ics.uci.edu/~ziv/ooad/intro_to_se/tsld008.htm", HIGH_RELEVANCE);
		seResults.put("http://www.ics.uci.edu/prospective/en/degrees/computer-science-engineering/", MODERATE_RELEVANCE);
		seResults.put("https://www.ics.uci.edu/faculty/area/area_software.php", MODERATE_RELEVANCE);
		seResults.put("http://www.ics.uci.edu/~icgse2016/", MODERATE_RELEVANCE);
		seResults.put("http://www.ics.uci.edu/~djr/DebraJRichardson/SE4S.html", LOW_RELEVANCE);
		seResults.put("http://se4s.ics.uci.edu/", LOW_RELEVANCE);
		seResults.put("http://www.ics.uci.edu/~icgse2016/2_0cfp.html", LOW_RELEVANCE);
		seResults.put("http://www.ics.uci.edu/~jajones/Informatics211-Fall2015.html", LOW_RELEVANCE);
		GOOGLE_RESULT_MAP.put(SOFTWARE_ENGINEERING, seResults);
		
		Map<String, Integer> secResults = new LinkedHashMap<String, Integer>();
		secResults.put("https://www.ics.uci.edu/faculty/area/area_security.php", HIGH_RELEVANCE);
		secResults.put("http://ccsw.ics.uci.edu/15/", HIGH_RELEVANCE);
		secResults.put("https://www.ics.uci.edu/computing/linux/security.php	", HIGH_RELEVANCE);
		secResults.put("http://sprout.ics.uci.edu/past_projects/odb/", MODERATE_RELEVANCE);
		secResults.put("http://www.ics.uci.edu/~projects/DataGuard/javadoc/org/itr_rescue/dataGuard/encryption/Security.html", MODERATE_RELEVANCE);
		secResults.put("http://sconce.ics.uci.edu/", MODERATE_RELEVANCE);
		secResults.put("https://www.ics.uci.edu/computing/linux/file-security.php", LOW_RELEVANCE);
		secResults.put("http://ccsw.ics.uci.edu/15/speakers.html", LOW_RELEVANCE);
		secResults.put("http://www.ics.uci.edu/~goodrich/teach/ics8/", LOW_RELEVANCE);
		secResults.put("http://www.ics.uci.edu/~keldefra/manet.htm", LOW_RELEVANCE);
		GOOGLE_RESULT_MAP.put(SECURITY, secResults);
		
		Map<String, Integer> saResults = new LinkedHashMap<String, Integer>();
		saResults.put("https://www.ics.uci.edu/about/search/search_sao.php", HIGH_RELEVANCE);
		saResults.put("http://www.ics.uci.edu/prospective/en/contact/student-affairs/", HIGH_RELEVANCE);
		saResults.put("http://www.ics.uci.edu/grad/sao/", HIGH_RELEVANCE);
		saResults.put("https://www.ics.uci.edu/ugrad/", MODERATE_RELEVANCE);
		saResults.put("http://www.ics.uci.edu/about/visit/", MODERATE_RELEVANCE);
		saResults.put("https://www.ics.uci.edu/grad/", MODERATE_RELEVANCE);
		saResults.put("https://www.ics.uci.edu/about/about_contact.php", LOW_RELEVANCE);
		saResults.put("https://www.ics.uci.edu/ugrad/sao/", LOW_RELEVANCE);
		saResults.put("https://www.ics.uci.edu/ugrad/sao/Appts_Walk-Ins.php", LOW_RELEVANCE);
		saResults.put("http://www.ics.uci.edu/ugrad/qa/", LOW_RELEVANCE);
		GOOGLE_RESULT_MAP.put(STUDENT_AFFAIRS, saResults);
		
		Map<String, Integer> gcResults = new LinkedHashMap<String, Integer>();
		gcResults.put("http://www.ics.uci.edu/grad/courses/", HIGH_RELEVANCE);
		gcResults.put("https://www.ics.uci.edu/grad/courses/details.php?id=440", HIGH_RELEVANCE);
		gcResults.put("http://www.ics.uci.edu/grad/courses/details.php?id=59", HIGH_RELEVANCE);
		gcResults.put("http://www.ics.uci.edu/grad/sao/", MODERATE_RELEVANCE);
		gcResults.put("https://www.ics.uci.edu/grad/courses/details.php?id=69", MODERATE_RELEVANCE);
		gcResults.put("https://www.ics.uci.edu/grad/degrees/degree_cs.php", MODERATE_RELEVANCE);
		gcResults.put("https://www.ics.uci.edu/grad/courses/listing.php?year=2015&level=ALL&department=CS&program=0BMC", LOW_RELEVANCE);
		gcResults.put("http://www.ics.uci.edu/grad/courses/details.php?id=521", LOW_RELEVANCE);
		gcResults.put("https://www.ics.uci.edu/grad/courses/details.php?id=62", LOW_RELEVANCE);
		gcResults.put("https://www.ics.uci.edu/grad/courses/details.php?id=110", LOW_RELEVANCE);
		GOOGLE_RESULT_MAP.put(GRADUATE_COURSES, gcResults);
		
		Map<String, Integer> clResults = new LinkedHashMap<String, Integer>();
		clResults.put("http://www.ics.uci.edu/~lopes/", HIGH_RELEVANCE);
		clResults.put("https://www.ics.uci.edu/faculty/profiles/view_faculty.php?ucinetid=lopes", HIGH_RELEVANCE);
		clResults.put("http://www.ics.uci.edu/~lopes/publications.html", HIGH_RELEVANCE);
		clResults.put("http://mondego.ics.uci.edu/", MODERATE_RELEVANCE);
		clResults.put("http://www.ics.uci.edu/~lopes/teaching/cs221W12/", MODERATE_RELEVANCE);
		clResults.put("https://www.ics.uci.edu/~lopes/patents.html", MODERATE_RELEVANCE);
		clResults.put("http://www.ics.uci.edu/~hsajnani/", LOW_RELEVANCE);
		clResults.put("http://luci.ics.uci.edu/blog/?p=416", LOW_RELEVANCE);
		clResults.put("http://luci.ics.uci.edu/blog/?tag=crista-lopes&paged=2", LOW_RELEVANCE);
		clResults.put("http://www.ics.uci.edu/~lopes/aop/aop-pics.html", LOW_RELEVANCE);
		GOOGLE_RESULT_MAP.put(CRISTA_LOPES, clResults);
		
		Map<String, Integer> restResults = new LinkedHashMap<String, Integer>();
		restResults.put("https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm", HIGH_RELEVANCE);
		restResults.put("https://www.ics.uci.edu/~fielding/pubs/dissertation/top.htm", HIGH_RELEVANCE);
		restResults.put("https://www.ics.uci.edu/~fielding/pubs/dissertation/introduction.htm", HIGH_RELEVANCE);
		restResults.put("https://www.ics.uci.edu/~fielding/pubs/dissertation/abstract.htm", MODERATE_RELEVANCE);
		restResults.put("http://www.ics.uci.edu/~fielding/talks/webarch_9805/", MODERATE_RELEVANCE);
		restResults.put("https://www.ics.uci.edu/~fielding/pubs/dissertation/conclusions.htm", MODERATE_RELEVANCE);
		restResults.put("https://asterixdb.ics.uci.edu/documentation/api.html", LOW_RELEVANCE);
		restResults.put("http://www.ics.uci.edu/~fielding/", LOW_RELEVANCE);
		restResults.put("https://www.ics.uci.edu/~fielding/pubs/dissertation/net_arch_styles.htm", LOW_RELEVANCE);
		restResults.put("https://asterix-gerrit.ics.uci.edu/Documentation/rest-api.html", LOW_RELEVANCE);
		GOOGLE_RESULT_MAP.put(REST, restResults);
		
		Map<String, Integer> cgResults = new LinkedHashMap<String, Integer>();
		cgResults.put("http://cgvw.ics.uci.edu/", HIGH_RELEVANCE);
		cgResults.put("http://www.ics.uci.edu/prospective/en/degrees/computer-game-science/", HIGH_RELEVANCE);
		cgResults.put("https://www.ics.uci.edu/ugrad/degrees/degree_cgs.php", HIGH_RELEVANCE);
		cgResults.put("http://cgvw.ics.uci.edu/affiliated-faculty/", MODERATE_RELEVANCE);
		cgResults.put("http://cgvw.ics.uci.edu/author/venita/", MODERATE_RELEVANCE);
		cgResults.put("http://cgvw.ics.uci.edu/author/admin/", MODERATE_RELEVANCE);
		cgResults.put("http://cgvw.ics.uci.edu/author/venita/", LOW_RELEVANCE);
		cgResults.put("https://www.ics.uci.edu/community/events/gameday/", LOW_RELEVANCE);
		cgResults.put("http://www.ics.uci.edu/~ebaumer/us12/", LOW_RELEVANCE);
		cgResults.put("https://www.ics.uci.edu/community/news/press/view_press?id=135", LOW_RELEVANCE);
		GOOGLE_RESULT_MAP.put(COMPUTER_GAMES, cgResults);
		
		Map<String, Integer> irResults = new LinkedHashMap<String, Integer>();
		irResults.put("http://www.ics.uci.edu/~lopes/teaching/cs221W12/", HIGH_RELEVANCE);
		irResults.put("http://www.ics.uci.edu/~lopes/teaching/cs221W13/", HIGH_RELEVANCE);
		irResults.put("http://www.ics.uci.edu/~djp3/classes/2014_01_INF141/calendar.html", HIGH_RELEVANCE);
		irResults.put("http://www.ics.uci.edu/~lopes/", MODERATE_RELEVANCE);
		irResults.put("http://www.ics.uci.edu/~djp3/classes/2009_01_02_INF141/calendar.html", MODERATE_RELEVANCE);
		irResults.put("http://www.ics.uci.edu/~djp3/classes/2014_01_INF141/structure.html", MODERATE_RELEVANCE);
		irResults.put("http://www.ics.uci.edu/~djp3/classes/2010_01_CS221/", LOW_RELEVANCE);
		irResults.put("http://www.ics.uci.edu/~kay/courses/i141/refs.html", LOW_RELEVANCE);
		irResults.put("http://www.ics.uci.edu/~djp3/classes/2009_01_02_INF141/", LOW_RELEVANCE);
		irResults.put("http://www.ics.uci.edu/~djp3/classes/2014_01_INF141/tasks/task_22/openlab.html", LOW_RELEVANCE);
		GOOGLE_RESULT_MAP.put(INFORMATION_RETRIEVAL, irResults);
		
		
	}
}
