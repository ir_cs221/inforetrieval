package edu.uci.inforetrieval.ndcg;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.uci.inforetrieval.crawler.MyCrawler;
import edu.uci.inforetrieval.search.SearchResult;
import edu.uci.inforetrieval.search.SearchService;
import edu.uci.inforetrieval.textprocessing.Utils;

@Service
public class NDCGAnalyzer {
	
	private static final int[] RELEVANCE_SCORES = {3, 3, 3, 2, 2, 2, 1, 1, 1, 1};
	
	@Autowired
	private SearchService searchService;
	
	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}
	
	public void analyzeNDCG() throws UnsupportedEncodingException, IOException{
		
		//while matching ignore the protocol http vs https
		//Check if one is subset of the other
		for(String query : QueryConstants.SAMPLE_QUERIES){
			List<SearchResult> ourResults = searchService.getRelevantDocuments(query, 5);
//			Map<String, Integer> googleResultMap = getGoogleResults(query, 10);
			Map<String, Integer> googleResultMap = QueryConstants.GOOGLE_RESULT_MAP.get(query);
			printResults(query, ourResults, googleResultMap);
			List<Integer> rankedDocs = new ArrayList<Integer>();
			for (SearchResult result : ourResults) {
				String ourUrl = result.getURL();
				Integer relevance = 0;
				for(Entry<String, Integer> entry : googleResultMap.entrySet()){
					if(compareURLs(ourUrl, entry.getKey())){
						relevance = entry.getValue();
						break;
					}
				}
				rankedDocs.add(relevance);
			}
			double ndcgValue = calculateNDCG(rankedDocs);
			System.out.println("NDCG for Query '" + query + "'is "+ndcgValue);
		}
	}
	
	public boolean compareURLs(String url1, String url2){
		url1 = url1.toLowerCase();
		url2 = url2.toLowerCase();
		
		url1 = Utils.removeProtocol(url1);
		url1 = Utils.removeWWW(url1);
		
		url2 = Utils.removeProtocol(url2);
		url2 = Utils.removeWWW(url2);
		
		if(url1.equals(url2)){
			return true;
		}
		//To equate http://ics.uci.edu/cs221/ with http://ics.uci.edu/cs221/index.html
		//we need to compare in following way
		if(url1.equals(url2.substring(0, url2.lastIndexOf('/') + 1)) ||
				url2.equals(url1.substring(0, url1.lastIndexOf('/') + 1 ))){
			return true;
		}
		return false;
	}
	
	
	
	private void printResults(String query, List<SearchResult> ourResults,
			Map<String, Integer> googleResultMap) {
		System.out.println("Query:"+query);
		System.out.println("Our Results:");
		for(SearchResult result : ourResults){
			System.out.println(result.getURL() + "; Title:" + result.getTitle()
					+ "; Score:"+result.getScore() + "; Page Rank:"+result.getPageRank());
		}
		System.out.println("Google Results:");
		for(Entry<String, Integer> entry : googleResultMap.entrySet()){
			System.out.println(entry.getKey()+"->"+entry.getValue());
		}
		System.out.println("---------------------------");
		
	}

	public double calculateNDCG(List<Integer> rankedDocs){
		double ndcg = 0;
		for (int i = 0; i < rankedDocs.size(); i++) {
			int relevance = rankedDocs.get(i);
			if(i == 0){
				ndcg += relevance;
			}else{
				double logValue = Math.log(i + 1) / Math.log(2);
				ndcg += ((double)relevance) / logValue;
			}
		}
		return ndcg;
	}

	Map<String, Integer> getGoogleResults(String query, int maxResults) throws UnsupportedEncodingException, IOException {
		
		Map<String, Integer> results = new LinkedHashMap<String, Integer>();
		String google = "http://www.google.com/search?q=";
		String charset = "UTF-8";
		String userAgent = "ics.uci.edu";
		int numResults = 0;
		query = query + "+site:ics.uci.edu";

		Elements links = Jsoup.connect(google + URLEncoder.encode(query, charset)).userAgent(userAgent).get().select(".g>.r>a");

		for (Element link : links) {
		    String title = link.text();
		    String url = link.absUrl("href"); // Google returns URLs in format "http://www.google.com/url?q=<url>&sa=U&ei=<someKey>".
		    url = URLDecoder.decode(url.substring(url.indexOf('=') + 1, url.indexOf('&')), "UTF-8");

		    if (!url.startsWith("http")) {
		        continue; // Ads/news/etc.
		    }
		    if(MyCrawler.FILTERS.matcher(url).matches()){
		    	continue;//Ignore the file extensions like pdf, doc etc
		    }

//		    System.out.println("Title: " + title);
//		    System.out.println("URL: " + url);
		    results.put(url.toLowerCase(), RELEVANCE_SCORES[numResults]);
		    numResults ++;
		    if(numResults == maxResults){
		    	break;
		    }
		}
		return results;
	}
}
