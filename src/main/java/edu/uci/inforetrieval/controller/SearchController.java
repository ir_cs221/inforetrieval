package edu.uci.inforetrieval.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.uci.inforetrieval.search.SearchResult;
import edu.uci.inforetrieval.search.SearchService;

@Controller
@RequestMapping("/search")
public class SearchController {
	
	@Autowired
	private SearchService searchService;
	
	@RequestMapping(value = "/results", method = RequestMethod.GET)
	public @ResponseBody List<SearchResult> getSearchResults(
			@RequestParam(value="query", required=true) String query,
			@RequestParam(value="maxresults", required = false, defaultValue="10") String maxResults ) 
					throws NumberFormatException, IOException{
		List<SearchResult> results = searchService.getRelevantDocuments(query, Integer.parseInt(maxResults));
		
		return results;
	}
	
}
