package edu.uci.inforetrieval.comparators;

import java.util.Comparator;

import edu.uci.inforetrieval.index.TermDocument;

public class TermFrequencyComparator implements Comparator<TermDocument> {

	@Override
	public int compare(TermDocument d1, TermDocument d2) {
		Integer termFreq2 = new Integer(d2.getTermFrequency());
		Integer termFreq1 = new Integer(d1.getTermFrequency());
		return termFreq2.compareTo(termFreq1);
	}

}
