package edu.uci.inforetrieval.comparators;

import java.util.Comparator;
import java.util.Map;

public class ValueComparator implements Comparator<String> {


	
	private Map<String, Integer> map;
	
	public ValueComparator(Map<String, Integer> map) {
		this.map = map;
	}

	@Override
	public int compare(String key1, String key2) {
		int retValue = map.get(key2).compareTo(map.get(key1));
		if(retValue != 0){
			return retValue;
		}else{
			return key2.compareTo(key1);
		}	
	}	

}
