package edu.uci.inforetrieval.comparators;

import java.util.Comparator;

public class KeyComparator  implements Comparator<String>{

	@Override
	public int compare(String key1, String key2) {
		return key1.compareTo(key2);
	}	
}
