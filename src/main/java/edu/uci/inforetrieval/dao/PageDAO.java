package edu.uci.inforetrieval.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.UnknownHostException;

import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import static edu.uci.inforetrieval.crawler.MongoDBConstants.*;

@Service
public class PageDAO {
	private static BufferedWriter bufferedWriter;
	
	static{
		
		try {
			bufferedWriter = new BufferedWriter(new FileWriter(FILE_DUMP));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void printAllPages() throws UnknownHostException{
		DBCollection collection = MongoClientFactory.getDB().getCollection(PAGE_TABLE_NAME);
		DBObject query = new BasicDBObject();
		
		long numDocs = collection.count(query);
		DBCursor dbCursor = collection.find(query);
		while(dbCursor.hasNext()){
			DBObject dbObject = dbCursor.next();
			System.out.println("URL:"+dbObject.get(URL));
		}
		System.out.println("Num Docs:"+numDocs);
		
		
	}
	
	public void insertIntoFile(String url, String html){
		
		try {
			bufferedWriter.write("URL:"+url+"\n");
			bufferedWriter.write(html+"\n");
			bufferedWriter.write(DELIMITER+"\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void insertPage(String url, String html, int textLength){
		DBCollection table = MongoClientFactory.getDB().getCollection(PAGE_TABLE_NAME);
	    BasicDBObject doc = new BasicDBObject();
	    doc.put(URL, url);
	    doc.put(HTML_CONTENT, html);
	    doc.put(TEXT_LENGTH, textLength);
	    table.insert(doc);
	}
	
	
	public long getNumDocs(){
		DBCollection table = MongoClientFactory.getDB().getCollection(PAGE_TABLE_NAME);
		DBObject query = new BasicDBObject();
		long numDocs = table.count(query);
		return numDocs;
	}
	
	public DBCursor getPageDBCursor(){
		DBCollection table = MongoClientFactory.getDB().getCollection(PAGE_TABLE_NAME);
		DBObject query = new BasicDBObject();
		DBCursor cursor = table.find(query);
		return cursor;
	}


	public void insertIntoDBFromFile(String htmlDumpFile) throws IOException{
		
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(htmlDumpFile));
			String next = null;
			StringBuilder htmlBuilder = new StringBuilder();
			String url = "";
			while((next = reader.readLine())!= null){
				if(next.startsWith("URL:")){
					url = next.substring(4);
					continue;
				}
				
				if(next.equals(DELIMITER)){
					if(url.startsWith("http") && !url.endsWith("csv")){
						insertPage(url, htmlBuilder.toString(), 0);
					}
					htmlBuilder.setLength(0);
					continue;
				}
				htmlBuilder.append(next);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		} finally{
			if(reader != null){
				reader.close();
			}
		}		
	}
	
	public void deletePages(String url){
		DBCollection table = MongoClientFactory.getDB().getCollection(PAGE_TABLE_NAME);
	    BasicDBObject doc = new BasicDBObject();
	    doc.put(URL, url);
	    table.remove(doc);
	}
	
	
	
}
