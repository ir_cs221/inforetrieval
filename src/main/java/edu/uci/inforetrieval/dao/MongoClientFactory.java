package edu.uci.inforetrieval.dao;

import static edu.uci.inforetrieval.crawler.MongoDBConstants.DB_HOST;
import static edu.uci.inforetrieval.crawler.MongoDBConstants.DB_NAME;
import static edu.uci.inforetrieval.crawler.MongoDBConstants.DB_PORT;

import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.MongoClient;

public class MongoClientFactory {
	private static MongoClient mongoClient;
	private static DB db;
	static{
		if(mongoClient == null){
			try {
				mongoClient = new MongoClient(DB_HOST, DB_PORT);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
		if(db == null){
			db = mongoClient.getDB(DB_NAME);
		}
	}
	public static MongoClient getMongoClient(){
		return mongoClient;
	}
	public static DB getDB(){
		return db;
	}
}
