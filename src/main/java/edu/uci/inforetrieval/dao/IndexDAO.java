package edu.uci.inforetrieval.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.lang.mutable.MutableInt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import static edu.uci.inforetrieval.crawler.MongoDBConstants.*;
import edu.uci.inforetrieval.index.TermDocument;
import edu.uci.inforetrieval.index.Term;
import edu.uci.inforetrieval.textprocessing.Utils;

@Service
public class IndexDAO {
	
	@Autowired
	private DocumentDAO documentDAO;
	
	public void setDocumentDAO(DocumentDAO documentDAO) {
		this.documentDAO = documentDAO;
	}

	public void insertIndexMap(Map<String, Term> indexMap){
		DB db = MongoClientFactory.getDB();
		DBCollection termTable = db.getCollection(TERM_TABLE_NAME);
		for(Entry<String, Term> entry : indexMap.entrySet()){
			Term term = entry.getValue();
			
			DBObject termDBObject = new BasicDBObject();
			termDBObject.put(TERM_ID, term.getId());
			termDBObject.put(WORD, term.getWord());

			BasicDBList dbList = new BasicDBList();
			
			List<TermDocument> termDocs = term.getDocuments();
			for(TermDocument termDoc : termDocs){
				Map<String, Object> documentMap = new TreeMap<String, Object>();
				documentMap.put(DOC_ID, termDoc.getDocID());
				documentMap.put(URL, termDoc.getUrl());
				documentMap.put(TERM_FREQUENCY, termDoc.getTermFrequency());
				documentMap.put(TERM_POSITIONS, termDoc.getTermPositions());
				documentMap.put(TITLE, termDoc.getTitle());
				documentMap.put(TF_IDF, Utils.calculateTF_IDF(termDoc.getTermFrequency(), termDocs.size()));
				documentMap.put(INDEX, termDoc.getTermIndex());
				documentMap.put(ANCHOR_TEXT_LIST, termDoc.getAnchorTextList());
				documentMap.put(PAGE_RANK, termDoc.getPageRank());
				dbList.add(documentMap);
			}
			
			termDBObject.put(DOCUMENTS, dbList);
			
			termTable.insert(termDBObject);
//			for(Entry<String, TermDocument> docEntry : term.getUrlVsDocuments().entrySet()){
//				TermDocument document = docEntry.getValue();
//				DBObject docDBObject = new BasicDBObject();
//				docDBObject.put(DOC_ID, document.getDocID());
//				docDBObject.put(URL, document.getUrl());
//				docDBObject.put(TERM_FREQUENCY, document.getTermFrequency());
//				docDBObject.put(TERM_POSITIONS, document.getTermPositions());
//				docDBObject.put(TF_IDF, Utils.calculateTF_IDF(document.getTermFrequency(), term.getDocIds().size()));
//				docTable.insert(docDBObject);
//			};
			
		}
	}
	
	public List<TermDocument> getTermDocuments(String word, int maxResults, MutableInt docFreq){
		List<TermDocument> termDocs = new ArrayList<TermDocument>();
		DB db = MongoClientFactory.getDB();
		DBCollection termTable = db.getCollection(TERM_TABLE_NAME);
		
		DBObject termObject = new BasicDBObject();
		termObject.put(WORD, word);
		DBObject result = termTable.findOne(termObject);
		if(result == null){
			return termDocs;
		}
		Integer termId = (Integer) result.get(TERM_ID);
		List<Map<String, Object>> documentList = (List<Map<String, Object>>) result.get(DOCUMENTS);
		int docFrequency = documentList.size();
		docFreq.add(docFrequency);
		
		int numResults = 0;
		for(Map<String, Object> map : documentList){
			if(maxResults > 0 && numResults >= maxResults){
				break;
			}
			numResults++;
			int docId = (Integer)map.get(DOC_ID);
			TermDocument document = new TermDocument((String)map.get(URL), docId);
			
			document.setTermPositions((List<Integer>) map.get(TERM_POSITIONS));
			document.setTermFrequency((int) map.get(TERM_FREQUENCY));
			document.setTf_idf((double) map.get(TF_IDF));
			document.setTitle((String)map.get(TITLE));
			document.setAnchorTextList((List<String>) map.get(ANCHOR_TEXT_LIST));
			document.setPageRank((double) map.get(PAGE_RANK));
			int termIndex = (int) map.get(INDEX);
			String relevantText = documentDAO.getRelevantText(docId, termIndex);
			document.setRelevantText(relevantText+"..." );
			termDocs.add(document);
		}
		return termDocs;
	}
}
