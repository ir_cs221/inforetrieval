package edu.uci.inforetrieval.dao;

import static edu.uci.inforetrieval.crawler.MongoDBConstants.*;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import edu.uci.inforetrieval.crawler.Document;

@Service
public class DocumentDAO {
	

	private static final int RELEVANT_TEXT_SIZE = 100;
	
	public void insertDocument(Document document){

		DBCollection table = MongoClientFactory.getDB().getCollection(DOCUMENT_TABLE_NAME);
	    BasicDBObject doc = new BasicDBObject();
	    doc.put(URL, document.getUrl());
	    doc.put(HTML_CONTENT, document.getHtmlContent());
	    doc.put(TEXT_LENGTH, document.getTextLength());
	    doc.put(TEXT_CONTENT, document.getTextContent());
	    doc.put(TITLE, document.getTitle());
	    doc.put(URL_CONTENT, document.getUrlContent());
	    doc.put(DOC_ID, document.getDocID());
	    table.insert(doc);
	
	}
	
	public DBCursor getDocumentDBCursor(){

		DBCollection table = MongoClientFactory.getDB().getCollection(DOCUMENT_TABLE_NAME);
		DBObject query = new BasicDBObject();
		DBCursor cursor = table.find(query);
		return cursor;
	
	}

	public String getRelevantText(int docID, int index) {
		DBCollection table = MongoClientFactory.getDB().getCollection(DOCUMENT_TABLE_NAME);
		DBObject query = new BasicDBObject();
		query.put(DOC_ID, docID);
		DBObject result = table.findOne(query);
		
		String textContent = (String) result.get(TEXT_CONTENT);
		if(index < 0 || index + RELEVANT_TEXT_SIZE > textContent.length() - 1){
			return "";
		}
		StringBuilder sb = new StringBuilder();
		int nextSpace = textContent.indexOf(' ', index);
//		sb.append("<b>");
//		String nextWord = textContent.substring(index, nextSpace);
//		sb.append(nextWord);
//		sb.append("</b>");
//		String remString = textContent.substring(nextSpace, index + RELEVANT_TEXT_SIZE);
//		sb.append(remString);
		String retString = textContent.substring(index, index + RELEVANT_TEXT_SIZE);
		return retString;

	}

	public void updateDocument_addAnchorTextList(Document doc) {

		DBCollection table = MongoClientFactory.getDB().getCollection(DOCUMENT_TABLE_NAME);
	    DBObject query = new BasicDBObject();
	    query.put(DOC_ID, doc.getDocID());
	    
		DBObject newFields = new BasicDBObject();	
		newFields.put(ANCHOR_TEXT_LIST, doc.getAnchorTextList());
		
		DBObject dbObject = new BasicDBObject();
		dbObject.put(SET_OPERATOR, newFields);
		//The update includes the new field "anchor_text_list" in the Document table
		table.update(query, dbObject);
		
	}

	public void deleteDoc(int docID) {
		DBCollection table = MongoClientFactory.getDB().getCollection(DOCUMENT_TABLE_NAME);
	    BasicDBObject doc = new BasicDBObject();
	    doc.put(DOC_ID, docID);
	    table.remove(doc);
		
	}
	
	public List<Document> getAllDocs(){
		DBCursor dbCursor = getDocumentDBCursor();

		List<Document> documents = new ArrayList<Document>();
		while(dbCursor.hasNext()){
			DBObject dbObject = dbCursor.next();
			String urlString = (String) dbObject.get(URL);
			String htmlContent = (String) dbObject.get(HTML_CONTENT);
			String text = (String) dbObject.get(TEXT_CONTENT);
			String title = (String) dbObject.get(TITLE);
			String urlContent = (String) dbObject.get(URL_CONTENT);
			double pageRank = 0;
			if( dbObject.get(PAGE_RANK) != null){
				pageRank = (double) dbObject.get(PAGE_RANK);
			};
			Document document = new Document();
			document.setHtmlContent(htmlContent);
			document.setTextContent(text);
			document.setTextLength(text.length());
			document.setTitle(title);
			document.setUrl(urlString);
			document.setUrlContent(urlContent);
			document.setPageRank(pageRank);
			
			documents.add(document);
			
		}
		return documents;
	}

	public void updateDocument_addPageRank(Document doc) {

		DBCollection table = MongoClientFactory.getDB().getCollection(DOCUMENT_TABLE_NAME);
	    DBObject query = new BasicDBObject();
	    query.put(DOC_ID, doc.getDocID());
	    
		DBObject newFields = new BasicDBObject();	
		newFields.put(PAGE_RANK, doc.getPageRank());
		
		DBObject dbObject = new BasicDBObject();
		dbObject.put(SET_OPERATOR, newFields);
		//The update includes the new field "page_rank" in the Document table
		table.update(query, dbObject);
		
	
		
	}
}
