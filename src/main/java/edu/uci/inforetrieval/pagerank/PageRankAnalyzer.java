package edu.uci.inforetrieval.pagerank;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.uci.inforetrieval.crawler.Document;
import edu.uci.inforetrieval.crawler.MyCrawler;
import edu.uci.inforetrieval.textprocessing.Utils;


public class PageRankAnalyzer {
	
	private static final double DAMPING_FACTOR = 0.85;
	private static final int NUM_ITERATIONS = 10;
	
	public void calculatePageRank(List<Document> docs){
		Map<String, Document> urlVsDoc = new LinkedHashMap<String, Document>();
		for(Document document : docs){
			String url = Utils.normalizeURL(document.getUrl());
			urlVsDoc.put(url, document);
		}
		for (Document document : docs) {
			org.jsoup.nodes.Document jsoupDoc = Jsoup.parse(document.getHtmlContent());
			Elements links = jsoupDoc.select("a[href]");
			int linkSize = 0;
			for(Element link : links){
				String absLink = link.attr("abs:href");
				absLink = absLink.toLowerCase();
				URL url;
				try {
					url = new URL(absLink);
				} catch (MalformedURLException e) {
					System.err.println("'"+absLink+"' is malformed");
					//Ignore exception and continue
					continue;
				}
				String host = url.getHost();
				//Ignore non-ics domains
				if(!host.toLowerCase().endsWith(MyCrawler.ICS_DOMAIN_SUFFIX)){
					continue;
				}
				linkSize++;
				String urlString = Utils.normalizeURL(absLink);
				Document outDoc = urlVsDoc.get(urlString);
				if(outDoc != null){
					outDoc.addIncomingDoc(Utils.normalizeURL(document.getUrl()));
				}
				
			}
			document.setNumOutGoingLinks(linkSize);
		}
		
		
		for (int i = 1; i <= NUM_ITERATIONS ; i++) {
			for(Document document : docs){
				double pageRank = (1 - DAMPING_FACTOR) + DAMPING_FACTOR * (calculateSum(urlVsDoc, document.getIncomingDocs()));
				document.setPageRank(pageRank);
			}
		}
		int print = 100;
		for (Document doc : docs) {
			System.out.println("URL:"+doc.getUrl()+"; PageRank:"+doc.getPageRank());
			if(print == 0){
				break;
			}
			print --;
		}
		
	}

	private double calculateSum(Map<String, Document> urlVsDoc,
			List<String> incomingDocs) {
		double retSum = 0;
		for (String url : incomingDocs) {
			Document doc = urlVsDoc.get(url);
			retSum += ((double)doc.getPageRank()) / ((double)doc.getNumOutGoingLinks());
		}
		return retSum;
	}
}
