package edu.uci.inforetrieval.pagerank;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.uci.inforetrieval.crawler.Document;
import edu.uci.inforetrieval.crawler.MyCrawler;
import edu.uci.inforetrieval.textprocessing.Utils;

public class AnchorTextAnalyzer {
	
	private Utils utils;
	
	public AnchorTextAnalyzer(){
		utils = new Utils();
	}
	
	public void populateAnchorText(List<Document> docs){
		Map<String, Set<String>> urlVsAnchorTextList = new HashMap<String, Set<String>>();
		for (Document document : docs) {
			org.jsoup.nodes.Document jsoupDoc = Jsoup.parse(document.getHtmlContent());
			Elements links = jsoupDoc.select("a[href]");
			for(Element link : links){
				String absLink = link.attr("abs:href");
				absLink = absLink.toLowerCase();
				String linkText = link.text().toLowerCase();
				URL url;
				try {
					url = new URL(absLink);
				} catch (MalformedURLException e) {
					System.err.println("'"+absLink+"' is malformed");
					//Ignore exception and continue
					continue;
				}
				String host = url.getHost();
				//Ignore non-ics domains
				if(!host.toLowerCase().endsWith(MyCrawler.ICS_DOMAIN_SUFFIX)){
					continue;
				}
				Set<String> anchorTextSet = urlVsAnchorTextList.get(absLink);
				if(anchorTextSet == null){
					anchorTextSet = new HashSet<String>();
					urlVsAnchorTextList.put(absLink, anchorTextSet);
				}
				anchorTextSet.add(linkText);
			}
		}
		for(Document document : docs){
			String url = document.getUrl().toLowerCase();
			if(urlVsAnchorTextList.containsKey(url)){
				Set<String> anchorTextSet = urlVsAnchorTextList.get(url);
				document.setAnchorTextList(new ArrayList<String>(anchorTextSet));
				System.out.println("The anchor text list for the URL:"+url);
				System.out.println(anchorTextSet+";/n");
			}else{
				System.out.println("Didn't crawl the ics url :"+url);
			}
		}
	}
}
