package edu.uci.inforetrieval.main;

import java.io.PrintStream;

import org.apache.log4j.Logger;

import edu.uci.inforetrieval.crawler.Controller;

public class Main {
	
	private static Logger LOG = Logger.getLogger(Main.class);
	private static final String SYSOUT_FILE = "sysout.log";
	
	
	public static void main(String[] args) throws Exception {
		
		System.setOut(new PrintStream(SYSOUT_FILE));
		Controller controller = new Controller();
		try {
			controller.crawl();
//			LOG.info("Crawling Finished");
			System.out.println("Crawling Finished");
		} catch (Exception e) {
//			LOG.error(e.getMessage(), e);
			e.printStackTrace();
			throw e;
		}
		
	}
}
