package edu.uci.inforetrieval.main;

import java.io.FileNotFoundException;
import java.net.UnknownHostException;

import edu.uci.inforetrieval.dao.PageDAO;

public class Test {
	
	private static final String SYSOUT_FILE = "sysout.log";
	
	public static void main(String[] args) throws FileNotFoundException {
//		System.setOut(new PrintStream(SYSOUT_FILE));
		try {
			PageDAO pageDAO = new PageDAO();
			pageDAO.insertPage("sample URL", "Sample HTML", 2);
			pageDAO.printAllPages();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
}
