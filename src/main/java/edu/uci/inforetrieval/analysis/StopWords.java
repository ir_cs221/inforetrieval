package edu.uci.inforetrieval.analysis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class StopWords {
	private static Set<String> stopWords;
	private static final String STOP_WORDS_TXT = "/stopwords.txt";
	
	public static Set<String> getStopWords() throws IOException{
		if(stopWords == null){
			try {
				loadStopWords();
			} catch (IOException e) {
				e.printStackTrace();
				throw e;
			}
		}
		return stopWords;
	}
	
	private static void loadStopWords() throws IOException {
		stopWords = new HashSet<String>();
		InputStream inputStream = StopWords.class.getResourceAsStream(STOP_WORDS_TXT);
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		String nextLine = null;
		while((nextLine = bufferedReader.readLine()) != null){
			stopWords.add(nextLine);
		}
		
	}

	
}
