package edu.uci.inforetrieval.analysis;

import static edu.uci.inforetrieval.crawler.MongoDBConstants.DELIMITER;
import static edu.uci.inforetrieval.crawler.MongoDBConstants.HTML_CONTENT;
import static edu.uci.inforetrieval.crawler.MongoDBConstants.URL;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.mongodb.DBCursor;
import com.mongodb.DBObject;

import edu.uci.inforetrieval.comparators.KeyComparator;
import edu.uci.inforetrieval.comparators.ValueComparator;
import edu.uci.inforetrieval.dao.PageDAO;
import edu.uci.inforetrieval.textprocessing.Utils;

public class DataAnalysis {
	
	private static final String SUB_DOMAIN_FILE = "src/main/resources/dataAnalysis/Subdomains.csv";
	private static final String FREQ_WORDS_FILE = "src/main/resources/dataAnalysis/freqWords.csv";
	private static final String FREQ_3_GRAMS_FILE = "src/main/resources/dataAnalysis/freq3Grams.csv";
	private static final String LONGEST_PAGE_FILE = "src/main/resources/dataAnalysis/longestPage.txt";
	private Utils utils;
	private PageDAO pageDAO;
	
	public void setPageDAO(PageDAO pageDAO) {
		this.pageDAO = pageDAO;
	}
	
	public void setUtils(Utils utils) {
		this.utils = utils;
	}
	
	public DataStatistics getDataStatistics() throws IOException{
		DataStatistics dataStatistics = new DataStatistics();
		DBCursor cursor = pageDAO.getPageDBCursor();
		
		while(cursor.hasNext()){
			DBObject dbObject  = cursor.next();
			String urlString = (String) dbObject.get(URL);
			String htmlContent = (String) dbObject.get(HTML_CONTENT);
			populateDataStatistics(urlString, htmlContent, dataStatistics);
		}
		return dataStatistics;
	}
	
	private void populateDataStatistics(String urlString, String htmlContent,
			DataStatistics dataStatistics) throws IOException {
		if(!utils.isValidPage(urlString)){
			return;
		}
		URL url = new URL(urlString);
		
		String host = url.getHost();
		dataStatistics.addSubDomain(host);
		
		//Populate LonngestPageURL and longestSoFar
		String text = utils.getText(htmlContent);
		String[] words = text.split("\\s+");
		if(words.length > dataStatistics.getNumWordsInlongestPage()){
			dataStatistics.setNumWordsInlongestPage(words.length);
			dataStatistics.setLongestPage(urlString);
		}
		
		//Populate High Frequency Words
		List<String> wordList = utils.getWords(text);
		for(String word: wordList){
			dataStatistics.addWord(word);
		}
		
		//Populate Three gram Words
		List<String> threeGrams = utils.get3Grams(wordList);
		for(String threeGram : threeGrams){
			dataStatistics.add3Gram(threeGram);
		}
	
		
	}

		
	public DataStatistics getDataStatistics_htmldump(String htmlDump) throws IOException{
		DataStatistics dataStatistics = new DataStatistics();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(htmlDump));
			String next = null;
			StringBuilder htmlBuilder = new StringBuilder();
			String url = "";
			while((next = reader.readLine())!= null){
				if(next.startsWith("URL:")){
					url = next.substring(4);
					continue;
				}
				
				if(next.equals(DELIMITER)){
					populateDataStatistics(url, htmlBuilder.toString(), 
								dataStatistics);
					htmlBuilder.setLength(0);
					continue;
				}
				htmlBuilder.append(next);
			}
			return dataStatistics;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw e;
		} finally{
			if(reader != null){
				reader.close();
			}
		}
		
	}
	
	public void analyzeData(DataStatistics dataStatistics) throws IOException{
		BufferedWriter bufferedWriter = null;
		try {
			Map<String, Integer> hashMap = dataStatistics.getSubDomainsVsNumPages();
			Map<String, Integer> treeMap = new TreeMap<String, Integer>(new KeyComparator());
			treeMap.putAll(hashMap);
			bufferedWriter = new BufferedWriter(new FileWriter(SUB_DOMAIN_FILE));
			bufferedWriter.write("URL, Number\n");
			Iterator<String> iterator = treeMap.keySet().iterator();
			while(iterator.hasNext()){
				String subDomain = iterator.next();
				Integer numPages = hashMap.get(subDomain);
				bufferedWriter.write("\""+subDomain+"\","+numPages+"\n");
			}
			bufferedWriter.close();
			
			bufferedWriter = new BufferedWriter(new FileWriter(FREQ_WORDS_FILE));
			hashMap = dataStatistics.getMostCommonWords();
			ValueComparator valueComparator = new ValueComparator(hashMap);
			treeMap = new TreeMap<String, Integer>(valueComparator);
			treeMap.putAll(hashMap);
			iterator = treeMap.keySet().iterator();
			int count = 0;
			bufferedWriter.write("Word, Frequency\n");
			while(iterator.hasNext()){
				String word = iterator.next();
				Integer freq = hashMap.get(word);
				bufferedWriter.write("\""+word + "\", " + freq+"\n");
				count++;
				if(count == 500){
					break;
				}
			}
			bufferedWriter.close();
			
			bufferedWriter = new BufferedWriter(new FileWriter(FREQ_3_GRAMS_FILE));
			hashMap = dataStatistics.getMostCommon3Grams();
			valueComparator = new ValueComparator(hashMap);
			treeMap = new TreeMap<String, Integer>(valueComparator);
			treeMap.putAll(hashMap);
			iterator = treeMap.keySet().iterator();
			count = 0;
			bufferedWriter.write("3 gram, Frequency\n");
			while(iterator.hasNext()){
				String threeGram = iterator.next();
				Integer freq = hashMap.get(threeGram);
				bufferedWriter.write("\""+threeGram + "\", " + freq+"\n");
				count++;
				if(count == 20){
					break;
				}
			}
			bufferedWriter.close();
			
			bufferedWriter = new BufferedWriter(new FileWriter(LONGEST_PAGE_FILE));
			bufferedWriter.write("Longest Page: "+dataStatistics.getLongestPage()+"\n");
			bufferedWriter.write("Number of Words in Longest Page: "+dataStatistics.getNumWordsInlongestPage());
			
			
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}finally{
			if(bufferedWriter != null){
				bufferedWriter.close();
			}
		}
	}
	
}
