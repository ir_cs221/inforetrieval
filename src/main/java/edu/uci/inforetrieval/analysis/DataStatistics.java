package edu.uci.inforetrieval.analysis;

import java.util.HashMap;
import java.util.Map;

public class DataStatistics {
	
	private int numUniquePages;
	private Map<String, Integer> subDomainsVsNumPages;
	private int numWordsInlongestPage;
	private String longestPage;
	private Map<String, Integer> mostCommonWords;
	private Map<String, Integer> mostCommon3Grams;
	
	public DataStatistics(){
		subDomainsVsNumPages = new HashMap<String, Integer>();
		mostCommonWords = new HashMap<String, Integer>();
		mostCommon3Grams = new HashMap<String, Integer>();
	}
	
	public int getNumUniquePages() {
		return numUniquePages;
	}
	public void setNumUniquePages(int numUniquePages) {
		this.numUniquePages = numUniquePages;
	}
	public Map<String, Integer> getSubDomainsVsNumPages() {
		return subDomainsVsNumPages;
	}
	public void setSubDomainsVsNumPages(Map<String, Integer> subDomainsVsPages) {
		this.subDomainsVsNumPages = subDomainsVsPages;
	}
	public void addSubDomain(String subDomain){
		subDomain = subDomain.toLowerCase();
		Integer numPages = subDomainsVsNumPages.get(subDomain);
		if(numPages == null){
			subDomainsVsNumPages.put(subDomain, 1);
		}else{
			subDomainsVsNumPages.put(subDomain, ++numPages);
		}
		
	}
	
	public int getNumWordsInlongestPage() {
		return numWordsInlongestPage;
	}
	public void setNumWordsInlongestPage(int numWordsInlongestPage) {
		this.numWordsInlongestPage = numWordsInlongestPage;
	}
	public String getLongestPage() {
		return longestPage;
	}
	public void setLongestPage(String longestPage) {
		this.longestPage = longestPage;
	}
	public Map<String, Integer> getMostCommonWords() {
		return mostCommonWords;
	}
	public void setMostCommonWords(Map<String, Integer> mostCommonWords) {
		this.mostCommonWords = mostCommonWords;
	}
	public void addWord(String word){
		word = word.toLowerCase();
		Integer freq = mostCommonWords.get(word);
		if(freq == null){
			mostCommonWords.put(word, 1);
		}else{
			mostCommonWords.put(word, ++freq);
		}
	}
	
	public Map<String, Integer> getMostCommon3Grams() {
		return mostCommon3Grams;
	}
	public void setMostCommon3Grams(Map<String, Integer> mostCommon3Grams) {
		this.mostCommon3Grams = mostCommon3Grams;
	}
	public void add3Gram(String threeGram){
		threeGram = threeGram.toLowerCase();
		Integer freq = mostCommon3Grams.get(threeGram);
		if(freq == null){
			mostCommon3Grams.put(threeGram, 1);
		}else{
			mostCommon3Grams.put(threeGram, ++freq);
		}
	}
	
	
}
