angular.module('myApp', []).controller('myController', function($scope, $http){
	$scope.showLoadingBar = false;
	$scope.getResults = function(){
		$scope.showLoadingBar = true;
		$scope.showResults = false;
		var config = {};
		var maxResults = 10;
		$http.get('/inforetrieval/rest/search/results?query='+$scope.queryText+"&maxresults="+maxResults, config).then(function(response){
			$scope.results = response.data;
			$scope.showResults = true;
			$scope.showLoadingBar = false;
		});
	}
});